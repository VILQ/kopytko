<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 24.06.13
 * Time: 00:47
 */
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title>Tables from MySQL Database</title>

    <style type="text/css">
        table.db-table 		{ border-right:1px solid #ccc; border-bottom:1px solid #ccc;width:500px }
        table.db-table th	{ background:#eee; padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
        table.db-table td	{ padding:5px; border-left:1px solid #ccc; border-top:1px solid #ccc; }
    </style>

</head>
<body>

<?php
/* connect to the db */
$connection = mysql_connect('localhost','vilq_konkursy','kopytko123');
mysql_select_db('vilq_konkursy',$connection);

/* show tables */
$result = mysql_query('SHOW TABLES',$connection) or die('cannot show tables');
while($tableName = mysql_fetch_row($result)) {

    $table = $tableName[0];

    echo '<h3>Struktura tabeli ',$table,'</h3>';
    $result2 = mysql_query('SHOW COLUMNS FROM '.$table) or die('cannot show columns from '.$table);
    if(mysql_num_rows($result2)) {
        echo '<table class="db-table" cellpadding="0" cellspacing="0"><thead><tr><th style="width:70px">Klucz</th><th style="width:100px">Typ</th><th>Opis</th></tr></thead><tbody>';
        while($row2 = mysql_fetch_row($result2)) {
            echo '<tr>';
            foreach($row2 as $key=>$value) {
                if (in_array($key,array(2,3,5)) ) continue;
                echo '<td>',$value,'</td>';
            }
            echo '</tr>';
        }
        echo '</tbody></table><br>';
    }
}
?>

</body>
</html>