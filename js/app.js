/**
 * Created with JetBrains PhpStorm.
 * User: VILQ
 * Date: 10.06.13
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */

var uid;
var u_id;
var u_name;
var u_fname;
var u_lname;
var u_mail;
var u_gender = '';
var answers = [];
var set_size = setInterval(function(){
    var  w = document.body.clientHeight;
    FB.Canvas.setSize({height: w});
}, 100);
var session = setInterval(function(){
    $.get('/check/'+app, function(data){
        if(data!='ok') {
            top.location.href = data;
        }
    });
},60000);
function goToPage(){
    var path = window.location.pathname.replace('app','home')
    window.location = path.substring(0, path.length - 1);
}
function checkterms() {
    if( typeof $.cookie('terms') != undefined && $.cookie('terms') == "true") {
        $("#terms").attr("checked","checked");
        setTimeout( function(){ $("#proceed").click(); }, 500);
    }
}
$(function(){
    $("#errorOK").live("click", function(){
        $(".modal-backdrop").fadeOut();
        $(".modal-backdrop-locked").fadeOut();
        $(this).parent().parent().fadeOut();
    });
    $(".modal-backdrop").on("click", function(e){
        $(".modal").fadeOut();
        $(this).fadeOut();
    });
    $("#terms").on("click", function(){
        var date = new Date();
        date.setTime(date.getTime() + (30000));
        $.cookie( $(this).attr('id'), ($(this).attr("checked")=="checked"), {expires:date} );
    });
});