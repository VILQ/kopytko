/**
 * Created with JetBrains PhpStorm.
 * User: VILQ
 * Date: 03.06.13
 * Time: 21:25
 */
function showMsg( msg ){
    $(".modal-backdrop").show();
    $("#errorMsg").html( msg );
    $("#error").css('top','100px');
    $("#continue").hide();
    $("#errorOK").show();
    $("#error").fadeIn('fast');
}
function loadData( data ){
    $("#content").html(data).trigger('create');
}
$(function(){
    $(".FBConnect").on("click", function(){
        FB.getLoginStatus(function(response) {
            if (response.status !== 'connected') {
                FB.login(function(response) {
                    if (response.authResponse) {
                        FB.api('/me', function(response) {
                            var access_token =   FB.getAuthResponse()['accessToken'];
                            $.post('/ajax/update',{data:response,token:access_token},function(data){
                                FB.api('/me/accounts?access_token='+access_token, function(response) {
                                    $.post('/ajax/pages',{data:response},function(data){
                                        window.location.reload();
                                    });
                                });
                            });
                        });
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                }, {scope: 'email,user_likes,user_about_me,publish_actions,manage_pages'});
            } else {
                FB.api('/me', function(response) {
                    var access_token =   FB.getAuthResponse()['accessToken'];
                    $.post('/ajax/update',{data:response,token:access_token},function(data){
                        FB.api('/me/accounts?access_token='+access_token, function(response) {
                            $.post('/ajax/pages',{data:response},function(data){
                                window.location.reload();
                            });
                        });
                    });
                });
            }
        });
    });
    $(".leftMenuItem").on("click", function(){
            window.location = '/panel/'+$(this).data('href'); //ajax vs reload
        $("#content").html('<div id="loader"></div>');
        $.get('/panel/'+$(this).data('href'),'',function(data){
            loadData( data )
        });
    });
    $("#settings").on("click",function(){
        FB.api('/me', function(response) {
            var access_token =   FB.getAuthResponse()['accessToken'];
            FB.api('/me/accounts?access_token='+access_token, function(response) {
                $.post('/ajax/pages',{data:response},function(data){  });
            });
        });
    });
    $(".application").live("click", function(){
        var type = $(this).data('id');
        $("#content").html('<div id="loader"></div>');
        $("#breadcrumb").html( '('+$(this).find('.description').html()+')' );
        $.get('/panel/new',{type:type},function(data){
            loadData( data )
        });
    });
    $("#appContinue").live('click', function(){
        var _me = $(this);
        $("#content").html('<div id="loader"></div>');
        $.get('/panel/edit',{app_id:_me.data('app')},function(data){
            loadData( data )
        });
    });
    $("#appDelete").live('click', function(){
        var _me = $(this);
        $("#content").html('<div id="loader"></div>');
        $.post('/panel/remove',{app:_me.data('app'),type:_me.data('mode')},function(data){
            loadData( data )
        });
    });
    $(".dashboard").live("click", function(){
        $("#breadcrumb").html( '('+$(this).find('.description').html()+')' );
        $("#content").html('<div id="loader"></div>');
        $.get('/panel/'+$(this).data('id'),'',function(data){
            loadData( data )
        });
    });
    $(".backButton").live("click", function(){
        $("#content").html('<div id="loader"></div>');
        $("#breadcrumb").html('');
        $.get('/panel/'+$(this).data('href'),'',function(data){
            loadData( data )
        });
    });
    $(".backButton2").live("click", function(){
        $("#content").html('<div id="loader"></div>');
        $.get('/panel/'+$(this).data('href'),'',function(data){
            loadData( data )
        });
    });
    $("#appListHolder .cButton").live("click", function(){
        var mode = $("#mode").val();
        var appid = $("#contestList").val();
        $("#content").html('<div id="loader"></div>');
        $.get('/panel/'+mode,{app_id:appid},function(data){
            loadData( data )
        });
    });
    $("#exportStatsToPdf").live("click", function(){
        $("#f_css").val( $("#statsHolder STYLE").html() );
        $("#f_header").val( $("#statsHeader").html() );
        $("#chart1").val( $("#chartUsers").html() );
        $("#chart2").val( $("#chartAnswers").html() );
        $("#chart3").val( $("#chartHourly").html() );
        $("#chart4").val( $("#chartLikes").html() );
        $("#comment1").val( $("#c_Users").html() );
        $("#comment2").val( $("#c_Answers").html() );
        $("#comment3").val( $("#c_Hourly").html() );
        $("#comment4").val( $("#c_Likes").html() );
        $("#pdfExportForm").submit();
    });
    $("#logout").on("click",function(){
        window.location = '/site/logout';
    });
    $("#appPreview").live("click", function(){
        $("#preview").val('true');
        $("#newAppForm").attr('target','prevewWindow');
        window.open("","prevewWindow","width=830,height=500,toolbar=0");
        var a = window.setTimeout(function(){
            $("#newAppForm").submit();
        },500);
        var b = window.setTimeout(function(){
            $("#preview").val('');
            $("#newAppForm").removeAttr('target');
        },1000);
    });
    setTimeout(function(){ $(".infoMsg").fadeOut('slow');},3000);
    $("#errorOK").on("click", function(){
        $(".modal-backdrop").fadeOut();
        $(".modal-backdrop-locked").fadeOut();
        $(this).parent().parent().fadeOut();
        setTimeout(function(){$(".modal").removeClass('wide');},500);
    });
    $(".modal-backdrop").on("click", function(e){
        $(".modal").fadeOut();
        $(this).fadeOut();
    });
});