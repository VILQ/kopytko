/**
 * Created with JetBrains PhpStorm.
 * User: VILQ
 * Date: 04.06.13
 * Time: 15:37
 * To change this template use File | Settings | File Templates.
 */

var addUrl = 'https://www.facebook.com/add.php?api_key=$KEY$&pages=1&page=$PAGE$';

var question = 1;
var reward = 1;

function nextAnswer( q,a ) {
    return '<div class="answer"><span class="delAnswer">-</span><input class="cInput" type="text" name="questions['+q+'][a]['+a+']" placeholder="Odpowiedź '+a+'"/> <input type="radio" name="good['+q+']" value="'+a+'" '+((a==1)?'checked="checked"':'')+'/>(prawidłowa)</div>';
}
function nextquestion( q ) {
    return '<div class="questions" id="q'+q+'" data-question="'+q+'"><div class="question"><input class="cInput" type="text" name="questions['+q+'][q]" placeholder="Pytanie '+q+'"/><span class="delQuestion">-</span><input name="questions['+q+'][s]" value="1" maxlength="2" class="qScore"/>(pkt)</div>'+nextAnswer(q,1)+'<span class="nextAnswer" data-q="'+q+'" data-a="2">+</span></div>';
}
function nextquestionsingle( q ) {
    return '<div class="questions single" id="q'+q+'" data-question="'+q+'"><div class="question"><input class="cInput" type="text" name="questions['+q+'][q]" placeholder="Pytanie/Temat '+q+'"/><span class="delQuestion">-</span></div>';
}
function createUploaders(){
    $(".jUploader").each(function(){
        var uploader = new qq.FileUploader({
            element: document.getElementById($(this).attr('id')),
            action: '/upload?view='+$(this).data('target'),
            target: $(this).data('target'),
            debug: false
        });
    });
}
function createUploader( id ){
    var uploader = new qq.FileUploader({
        element: document.getElementById('u_'+id),
        action: '/upload?view='+id,
        target: id,
        debug: false
    });
}
function afterUpload(result, target){
    var preview = $("#p_"+target);
    var previewPhoto = $("#pp_"+target+" IMG");
    var previewSize = $("#ps_"+target);
    var photoFile = $("#f_"+target);
    var fileUploader = $("#u_"+target);
    var fileLimit = $("#l_"+target)
    if (result.success === true){
        $(".modal-backdrop-locked").hide();
        $(".modal-spinner").hide();
        previewPhoto.attr('src', '/'+result.name);
        photoFile.val(result.name);
        previewSize.html('('+result.size+')');
        fileUploader.hide();
        fileLimit.hide();
        preview.show();
    } else {
        $(".modal-backdrop-locked").hide();
        $(".modal-spinner").hide();
        showMsg( result.errors );
    }
}
function validate(){
    if ( $("#appName").val() == '' ){
        $("#nAG").click();
        showMsg( 'Podaj nazwę swojej aplikacji' );
        return false;
    }
    if ( $("#"+$("#contestType").val()+"_start").val() == $("#"+$("#contestType").val()+"_end").val() ){
        $("#nAG").click();
        showMsg( 'Data końca publikacji musi być późniejsza niż początku.' );
        return false;
    }
    if ( $("#appFp").val() == '0' ){
        $("#nAG").click();
        showMsg( 'Wybierz fanpage' );
        return false;
    }
    if ( $("#termsUrl").val() == '' ){
        $("#nAG").click();
        showMsg( 'Wstaw link do regulaminu' );
        return false;
    }
    return true;
}
function urldecode(str) {
    return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}
$(function(){
    createUploaders();
    $("#newAppMenu SPAN").on('click', function(){
        var show = $("#"+$(this).data('tab'));
        $("#newAppMenu SPAN").removeClass('active');
        $(this).addClass('active');
        $(".newAppTabs").hide();
        show.fadeIn();
    });
    $(".delInfo").on("click", function(){
        var target = $(this).data('id');
        $("#f_"+target).val('');
        $("#u_"+target).show();
        $("#l_"+target).show();
        $("#p_"+target).hide();
    });
    $(".nextAnswer").live('click', function(){
        var q = parseInt( $(this).data('q') );
        var a = parseInt( $(this).data('a') );
        $("#q"+q).append(nextAnswer(q,a));
        $(this).data('a',a+1);
    });
    $(".delAnswer").live('click', function(){
        $(this).parent().remove();
    });
    $(".delQuestion").live('click', function(){
        $(this).parent().parent().remove();
        $("#qLimit").val( $("#newAppQuestion").find('.question').length );
    });
    $("#qLimit").live('change',function(){
        if ($(this).val() > $("#newAppQuestion").find('.question').length || isNaN(parseInt($(this).val()))){
            $(this).val($("#newAppQuestion").find('.question').length);
        }
    });
    $("#newAppQuestion .nextQuestion").bind('click', function(){
        question++;
        $("#newAppQuestion").append(nextquestion(question));
        $("#qLimit").val( $("#newAppQuestion").find('.question').length );
    });
    $("#newAppQuestion .nextQuestionSingle").bind('click', function(){
        question++;
        $("#newAppQuestion").append(nextquestionsingle(question));
    });
    $("#adv").live('click', function(){
        $(this).hide();
        $("#advanced").fadeIn();
    });
    $("#limits INPUT").live('click', function(){
        if( $(this).val()=='i' || $(this).hasClass('qScore') ){
            $("#minimum").fadeIn();
        } else {
            $("#minimum").fadeOut();
        }
    });
    $("#appSubmit").on('click', function(e){
        var _me = $(this);
        e.preventDefault();
        e.stopPropagation();
        if ( !validate() ){
            return false;
        }
        $("#dateFormat").removeAttr('name');
        $("#rangeFormat").removeAttr('name');
        $("#timeFormat").removeAttr('name');
        $("#rangeNames").removeAttr('name');
        $("#monthNames").removeAttr('name');
        $("#dayNames").removeAttr('name');
        $("#descriptions").removeAttr('name');

        if ( _me.val() == "Publikacja" ){
            window.open(addUrl.replace('$KEY$',$("#app_id").val()).replace("$PAGE$",$("#appFp").val()),'_blank','width=810,height=500,top=0,left=0,menubar=no,status=0,titlebar=0');
        }
        $("#newAppForm").submit();
    });
    $("#deleteApp").on('click', function(){
        $.post('/panel/delete',{app:$(this).data('app')}, function(data){
            if( data == 'ok'){
                window.location = '/panel';
            }
        });
    });
    $("#addReward").on('click', function(){
        $.post('/panel/newreward',{reward:++reward},function(data){
            $("#newAppRewards").append(data);
            createUploader('reward'+reward)
        });
    });
    $(".delReward").live('click', function(){
        $(this).parent().remove();
    });
    $(".scoreDelete").live('click', function(){
        $.post('/panel/deletevote',{id:$(this).data('id')}, function(data){
            if (data != ''){
                $("#newAppParticipants").html(data);
            }
        });
    });
    $(".show_story IMG").live('click', function(){
        $(".modal").addClass('wide');
       showMsg( urldecode($(this).parent().data('content')) );
    });
});