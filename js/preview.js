/**
 * Created with JetBrains PhpStorm.
 * User: VILQ
 * Date: 15.06.13
 * Time: 23:55
 * To change this template use File | Settings | File Templates.
 */
function showMsg( msg ){
    $(".modal-backdrop").show();
    $("#errorMsg").html( msg );
    $("#error").css('top','100px');
    $("#continue").hide();
    $("#errorOK").show();
    $("#error").fadeIn('fast');
}
function urldecode(str) {
    return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}
$(function(){
    $(window).on("resize", function(){
        window.resizeTo(850,768);
    });
    $(window).on("load", function(){
        window.resizeTo(850,768);
    });
    $("#welcome").css('display','block');
    $("#menu").hide();
    $("#topmenu .cButton").on("click", function(){
        $(".cButton").removeClass('active');
        $(this).addClass('active');
        $(".screens").hide();
        $("#"+$(this).data('tab')).fadeIn();
        $(".menuItem").removeClass('active');
        $("#m"+$(this).data('tab')).addClass('active');
        if ($(this).data('tab') == "welcome"){
            $("#menu").hide();
        } else {
            $("#menu").show();
        }
    });
    $(".qAnswer").on('click', function(){
        $(this).find('input').attr('checked','checked');
        var tmp = $(this).parent().attr('id');
        $("#"+tmp+" .nextQuestion").css('display','inline-block');
    });
    $("#proceed").on("click",function(e){
        if ( !$('#term').is(':checked') ) {
            e.preventDefault();
            $(".modal-backdrop").show();
            $("#errorMsg").html('Aby kontynuować musisz zaakceptować regulamin' );
            $("#error").css('top','100px');
            $("#continue").hide();
            $("#errorOK").show();
            $("#error").fadeIn('fast');
            return false;
        } else {
            $("#appButton").click();
        }
    });
    $("#errorOK").on("click", function(){
        $(".modal-backdrop").fadeOut();
        $(".modal-backdrop-locked").fadeOut();
        $(this).parent().parent().fadeOut();
    });
    $(".modal-backdrop").on("click", function(e){
        $(".modal").fadeOut();
        $(this).fadeOut();
    });
    $("#qsh .btn2").on("click", function(){
        $(".qHolder").hide();
        $("#h1").fadeIn();
    });
    $(".cArea").keypress(function(){
        if( $(this).val().length >= 5 && $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).data('q') != '0'){
            $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).css('display','inline-block');
        } else {
            $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).css('display','none');
        }
    });
    $("#qSelect").on("change", function(){
        $("#n2").attr('data-q',$(this).val());
        if( $("#text1").val().length >= 5){
            $("#n2").css('display','inline-block');
        } else {
            $("#n2").css('display','none');
        }
    });
    $(".show_story IMG").on('click', function(){
        $(".modal").addClass('wide');
        showMsg( urldecode($(this).parent().data('content')) );
    });
});