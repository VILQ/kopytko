/**
 * Created with JetBrains PhpStorm.
 * User: VILQ
 * Date: 11.06.13
 * Time: 11:45
 * To change this template use File | Settings | File Templates.
 */
function u_connect(){
    $.post('/manage',{app_id:app,u_id:u_id,u_fname:u_fname,u_lname:u_lname,u_mail:u_mail,u_gender:u_gender},function(data){});
}
function qStart(){
    $.post('/start',{app_id:app,u_id:u_id},function(data){});
}
function qStop(){
    $.post('/stop',{app_id:app,u_id:u_id,answers:answers},function(data){
        $("#finishScore").html(data);
    });
}
function showMsg( msg ){
    $(".modal-backdrop").show();
    $("#errorMsg").html( msg );
    $("#error").css('top','100px');
    $("#continue").hide();
    $("#errorOK").show();
    $("#error").fadeIn('fast');
}
function urldecode(str) {
    return decodeURIComponent((str+'').replace(/\+/g, '%20'));
}
$(function(){
    FB.Event.subscribe('auth.authResponseChange', function(){
        FB.api('/me',function(r){
            u_id = r.id;
            u_name = r.name;
            u_fname = r.first_name;
            u_lname = r.last_name;
            u_mail = r.email;
            u_gender = r.gender;
            u_connect();
        });
    });
    $(".menuItem").on("click", function(){
        var _me = $(this);
        current = _me.attr('id');
        var body = $("#appBody");
        var spinner = $("#spinner");
        var old = body.html();
        body.hide();
        spinner.show();
        $(".menuItem").removeClass('active');
        var base =  window.location.pathname.replace(app+'/','')+'/'+app+'/';
        console.log(base)
        $.get(base+_me.data('step'),{},function(data){
            if (data){
                body.html(data);
                spinner.hide();
                body.fadeIn();
                _me.addClass('active');
            } else {
                spinner.hide();
                body.show();
            }
        }).fail( function(){
                body.html('<div class="msg error">Nie można załadować strony...</div>');
                spinner.hide();
                body.show();
            });
    });
    $("#qsh .btn2").live("click", function(){
        qStart();
        answers = [];
        $(".qHolder").hide();
        $("#h1").fadeIn();
    });
    $(".nextQuestion").live("click", function(){
        if (typeof $("input:radio[name ='"+$(this).data('q')+"']:checked").val() != 'undefined'){
            answers.push($(this).data('q').replace('q','')+'|'+$("input:radio[name ='"+$(this).data('q')+"']:checked").val() );
        } else {
            if (typeof $("#qSelect").val() != 'undefined'){
                answers.push($(this).data('q').replace('q','')+'|'+$("#text1").val());
            }
            else {
                answers.push($(this).data('q').replace('q','')+'|'+$("#text"+$(this).data('q').replace('q','')).val());
            }
        }
        $(".qHolder").hide();
        var next = $("#h"+$(this).attr("id").replace('n',''));
            next.fadeIn();
        if ( next.hasClass('finish') ){
            qStop();
        }
    });
    $(".qAnswer").live('click', function(){
        $(this).find('input').attr('checked','checked');
        var tmp = $(this).parent().attr('id');
        $("#"+tmp+" .nextQuestion").css('display','inline-block');
    });
    $(".cArea").keypress(function(){
            if( $(this).val().length >= 5 && $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).data('q') != '0'){
                $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).css('display','inline-block');
            } else {
                $("#n"+(parseInt($(this).attr('id').replace('text',''))+1)).css('display','none');
            }
    });
    $("#qSelect").live("change", function(){
        $("#n2").attr('data-q',$(this).val());
        if( $("#text1").val().length >= 5){
            $("#n2").css('display','inline-block');
        } else {
            $("#n2").css('display','none');
        }
    });
    $(".show_story IMG").live('click', function(){
        $(".modal").addClass('wide');
        showMsg( urldecode($(this).parent().data('content')) );
    });
});