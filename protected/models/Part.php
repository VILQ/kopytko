<?php

/**
 * This is the model class for table "part".
 *
 * The followings are the available columns in table 'part':
 * @property integer $id
 * @property integer $contest_id
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $email
 * @property string $fb_id
 * @property integer $terms
 * @property string $created
 * @property integer $vote_lock
 *
 * The followings are the available model relations:
 * @property Answers[] $answers
 * @property Contest $contest
 */
class Part extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Part the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'part';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contest_id, terms, vote_lock', 'numerical', 'integerOnly'=>true),
			array('first_name, last_name, gender', 'length', 'max'=>50),
			array('email', 'length', 'max'=>255),
			array('fb_id', 'length', 'max'=>32),
			array('created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contest_id, first_name, last_name, gender, email, fb_id, terms, created, vote_lock', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answers', 'user_id'),
			'contest' => array(self::BELONGS_TO, 'Contest', 'contest_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contest_id' => 'Contest',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'gender' => 'Gender',
			'email' => 'Email',
			'fb_id' => 'Fb',
			'terms' => 'Terms',
			'created' => 'Created',
			'vote_lock' => 'Vote Lock',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contest_id',$this->contest_id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fb_id',$this->fb_id,true);
		$criteria->compare('terms',$this->terms);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('vote_lock',$this->vote_lock);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}