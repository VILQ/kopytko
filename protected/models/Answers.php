<?php

/**
 * This is the model class for table "answers".
 *
 * The followings are the available columns in table 'answers':
 * @property integer $id
 * @property integer $user_id
 * @property integer $contest_id
 * @property string $created
 * @property string $data
 * @property integer $sort_int
 * @property double $sort_float
 * @property string $sort_string
 *
 * The followings are the available model relations:
 * @property Contest $contest
 * @property Part $user
 */
class Answers extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Answers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, contest_id, sort_int', 'numerical', 'integerOnly'=>true),
			array('sort_float', 'numerical'),
			array('created, data, sort_string', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, contest_id, created, data, sort_int, sort_float, sort_string', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contest' => array(self::BELONGS_TO, 'Contest', 'contest_id'),
			'user' => array(self::BELONGS_TO, 'Part', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'contest_id' => 'Contest',
			'created' => 'Created',
			'data' => 'Data',
			'sort_int' => 'Sort Int',
			'sort_float' => 'Sort Float',
			'sort_string' => 'Sort String',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('contest_id',$this->contest_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('sort_int',$this->sort_int);
		$criteria->compare('sort_float',$this->sort_float);
		$criteria->compare('sort_string',$this->sort_string,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}