<?php

/**
 * This is the model class for table "contest".
 *
 * The followings are the available columns in table 'contest':
 * @property integer $id
 * @property integer $user_id
 * @property string $app_id
 * @property string $fanpage
 * @property string $name
 * @property integer $sandbox
 * @property integer $like
 * @property string $info
 * @property string $view
 * @property string $questions
 * @property string $answers
 * @property string $rewards
 * @property string $terms
 * @property string $date_start
 * @property string $date_end
 * @property string $type
 * @property string $data
 *
 * The followings are the available model relations:
 * @property Answers[] $answers0
 * @property User $user
 * @property ContestParams[] $contestParams
 * @property Part[] $parts
 */
class Contest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, sandbox, like', 'numerical', 'integerOnly'=>true),
			array('type', 'length', 'max'=>5),
			array('app_id, fanpage, name, info, view, questions, answers, rewards, terms, date_start, date_end, data', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, app_id, fanpage, name, sandbox, like, info, view, questions, answers, rewards, terms, date_start, date_end, type, data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers0' => array(self::HAS_MANY, 'Answers', 'contest_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'contestParams' => array(self::HAS_MANY, 'ContestParams', 'contest_id'),
			'parts' => array(self::HAS_MANY, 'Part', 'contest_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'app_id' => 'App',
			'fanpage' => 'Fanpage',
			'name' => 'Name',
			'sandbox' => 'Sandbox',
			'like' => 'Like',
			'info' => 'Info',
			'view' => 'View',
			'questions' => 'Questions',
			'answers' => 'Answers',
			'rewards' => 'Rewards',
			'terms' => 'Terms',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
			'type' => 'Type',
			'data' => 'Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('app_id',$this->app_id,true);
		$criteria->compare('fanpage',$this->fanpage,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('sandbox',$this->sandbox);
		$criteria->compare('like',$this->like);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('questions',$this->questions,true);
		$criteria->compare('answers',$this->answers,true);
		$criteria->compare('rewards',$this->rewards,true);
		$criteria->compare('terms',$this->terms,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('data',$this->data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function params() {
        $params = ContestParams::model()->findAllByPk( $this->id );
        $return = array();
        foreach( $params as $param ) {
            $return[$param['name']] = $param['value'];
        }
        return $return;
    }

    public function fb_name( $fb_id ) {
        return Pages::model()->find('fb_id ='.$fb_id)->name;
    }
}