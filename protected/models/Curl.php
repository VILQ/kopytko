<?php

class Curl
{
    private static $base_url = 'https://graph.facebook.com/';

    public static function get($url) {
        $ch = curl_init(self::$base_url.$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
        $output = curl_exec($ch);
        curl_close ($ch);
        return $output;
    }
}