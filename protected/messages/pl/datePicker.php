<?php
/**
 * Created by JetBrains PhpStorm.
 * User: gwilczynski
 * Date: 23.04.13
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */

return array(
    'Apply' => 'Zapisz',
    'Clear' => 'Wyczyść',
    'From' => 'Od',
    'To' => 'Do',
    'Custom Range' => 'Własny zakres',
    'Last 7 Days' => 'Ostatnie 7 dni',
    'Last 30 Days' => 'Ostatnie 30 dni',
    'This Month' => 'Aktualny miesiąc',
    'Last Month' => 'Ostatni miesiąc',
    'Su' => 'Nd',
    'Mo' => 'Po',
    'Tu' => 'Wt',
    'We' => 'Śr',
    'Th' => 'Cz',
    'Fr' => 'Pt',
    'Sa' => 'So',
    'Hour' => 'Godzina',
    'Minute' => 'Minuta',
    'Time Zone' => 'Strefa Czasowa',
    'Now' => 'Teraz',
    'Done' => 'OK',
    'January' => 'Styczeń',
    'February' => 'Luty',
    'March' => 'Marzec',
    'April' => 'Kwiecień',
    'May' => 'Maj',
    'June' => 'Czerwiec',
    'July' => 'Lipiec',
    'August' => 'Sierpień',
    'September' => 'Wrzesień',
    'October' => 'Październik',
    'November' => 'Listopad',
    'December' => 'Grudzień',
);