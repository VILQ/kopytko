DatePicker = {
    currentValue:null,
    selectors: new Array('.datepicker', '.datetimepicker', '.rangepicker'),
    dp_dateFormat:null,
    dt_timeFormat:null,
    dr_dateFormat:null,
    dayNames:null,
    monthNames:null,
    rangeDayNames:null,
    descriptions:null,
    _me:null,
    init: function(){
        _me = this;
        _me.dp_dateFormat = $("#dateFormat").val();
        _me.dt_timeFormat = $("#timeFormat").val();
        _me.dr_dateFormat = $("#rangeFormat").val();
        _me.dayNames      = $("#dayNames").val().replace(/"/g,'').replace("[",'').replace("]","").split(",");
        _me.monthNames    = $("#monthNames").val().replace(/"/g,'').replace("[",'').replace("]","").split(",");
        _me.descriptions  = $("#descriptions").val().replace(/"/g,'').replace("[",'').replace("]","").split(",");
        _me.rangeDayNames = _me.dayNames;
        _me.rangeDayNames.push( _me.rangeDayNames.shift() );
        $.each(_me.selectors, function(idx,vals){
            $(vals).each(function(){
                var val = $(this);
                $(val).val( _me.checkForCookie( val ) );
                $(val).on("change", function(){
                    if ( $(val).val() != _me.currentValue ){
                        _me.currentValue = $(val).val();
                        var tmp = $(val).data('ref').split('|');
                        var t = _me[tmp[0]];
                        var params = [];
                        params.push ( tmp[1] );
                        params.push ( $(val).attr('id') );
                        params.push ( $(val).val() );
                        if ( typeof t == 'function' ) {
                            t.call(val,params);
                        } else {
                            var data = {};
                            data[$(val).attr('id')] = $(val).val();
                            _me.request( tmp[0], data );
                        }
                    }
                });
            });
        });
        $(_me.selectors[0]).each(function(){
            $(this).datepicker({
                dateFormat: _me.dp_dateFormat,
                firstDay: 1,
                showSecond: false,
                defaultTime: $(this).val(),
                showMeridian: false,
                dayNamesMin: _me.dayNames,
                monthNames: _me.monthNames
            });
        });
        $(_me.selectors[0]).on("click", function(){
            $(this).datepicker('show');
        });
        $(_me.selectors[1]).each(function(){
            var tmp = $(this).data('ref').split('|');
            $(this).datetimepicker({
                dateFormat: _me.dp_dateFormat,
                timeFormat: _me.dt_timeFormat,
                firstDay: 1,
                showSecond: false,
                controlType: 'select',
                defaultTime: $(this).val(),
                dayNamesMin: _me.dayNames,
                monthNames: _me.monthNames,
                showTime: false,
                showTimezone: false,
                hourText: _me.descriptions[0],
                minuteText: _me.descriptions[1],
                timezoneText: _me.descriptions[2],
                currentText: _me.descriptions[3],
                closeText: _me.descriptions[4],
                defaultTimezone: tmp[2]
            });
        });
        $(_me.selectors[1]).on("click", function(){
            $(this).datetimepicker('show');
        });
        $(_me.selectors[2]).each(function(){
            var rangeNames = $("#rangeNames").val().replace(/"/g,'').replace("[",'').replace("]","").split(",");
            var tmpRanges = {};
            tmpRanges[rangeNames[5]]= [Date.today().add({ days: -6 }), 'today'];
            tmpRanges[rangeNames[6]]= [Date.today().add({ days: -29 }), 'today'];
            tmpRanges[rangeNames[7]]= [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()];
            tmpRanges[rangeNames[8]]= [Date.today().moveToFirstDayOfMonth().add({ months: -1 }), Date.today().moveToFirstDayOfMonth().add({ days: -1 })];
            $(this).daterangepicker({
                    ranges: tmpRanges,
                    locale: {
                        applyLabel: rangeNames[0],
                        clearLabel: rangeNames[1],
                        fromLabel:  rangeNames[2],
                        toLabel:    rangeNames[3],
                        customRangeLabel: rangeNames[4],
                        daysOfWeek: _me.rangeDayNames,
                        monthNames: _me.monthNames
                    },
                    format: _me.dr_dateFormat
                },
                function(start, end) {
                    $( this.element ).val( start.toString(_me.dr_dateFormat) + ' - ' + end.toString(_me.dr_dateFormat) );
                    $( this.element ).trigger( 'change' );
                });
        });
        $(_me.selectors[2]).on("click", function(){
            $(this).daterangepicker('show');
        });
    },
    none: function( data ){
        //console.log(data);
    },
    cookie: function ( data ){
        $.cookie( data[0] + '|' + data[1], data[2]);
    },
    checkForCookie: function( _that ){
        if ( $( _that ).data('ref') ) {
            var tmp = $( _that ).data('ref').split('|');
            return $.cookie( tmp[1] + '|' +  $( _that ).attr('id') ) || $( _that ).val();
        }
        return $( _that ).val()+'a';
    },
    session: function( data ){
        _me.cookie( data );
        $.ajax({
            url: '/ajax/setSessionVariable',
            type: 'post',
            data: {
                data: data
            },
            success: function(data){
                console.log("ok ",data);
            },
            error: function(data) {
                console.log("error ",data);
            }
        });
    },
    request: function( url, data ) {
        $.ajax({
            url: '/ajax/'+url,
            type: 'post',
            data: {
                data: data
            },
            success: function(data){
                console.log(data);
            },
            error: function(data) {
                console.log("error ",data);
            }
        });
    }
};
$(function(){
    DatePicker.init();
});