<?php

/**
 * Class datePicker
 *
 * author: gregory.wilczynski@gmail.com
 *
 * widget usage:
 *
 * Yii::import('application.extensions.datePicker.datePicker');
 *   $this->widget('datePicker',array(
 *       'label' => 'string' // label for datepickerfield
 *       'mode' => 'date', // picker mode: date,datetime,range
 *       'field' => 'date', // input field ID/name
 *       'store' => 'function / ajax request', // js onChange call
 *       'from' => date("d-m-Y", time() - 7 * 60 * 60 * 24) // default date from (for range): eg. -7 days
 *       'to' => date("d-m-Y", time() - 60 * 60 * 24) // default date to: eg. yesterday
 *       'zone' => 0 // timezone offset
 *       'nl' => false/true // start widget from new line
 *   ));
 *
 *      'store' predefined functions:
 *          cookie - sets cookie: "controler.view|fieldID" and value of current field,
 *          session - sets session variable: "controler.view|fieldID" and value of current field and sets cookie as above,
 *
 */
class datePicker extends CWidget {

    /**
     * @var string
     */
    public $mode = 'date';
    /**
     * @var string
     */
    public $field = '';
    /**
     * @var string
     */
    public $alt = '|';
    /**
     * @var string
     */
    public $store = 'none';

    /**
     * @var null
     */
    public $from = null;
    /**
     * @var null
     */
    public $to = null;
    /**
     * @var int
     */
    public $zone = 0;

    /**
     * @var string
     */
    public $label = '';
    /**
     * @var bool
     */
    public $nl = false;
    /**
     * @var string
     */
    private $language;

    /**
     * @throws CException
     */
    public function init() {
            if(!in_array($this->mode, array('date','range','datetime')))
                    throw new CException('unknow mode "'.$this->mode.'"');
            if(!isset($this->language))
                    $this->language=Yii::app()->language;
    }
    /**
     * @param $string
     * @return string
     */
    private function unicodeToUtf($string){
        return str_replace('\\','',html_entity_decode(preg_replace('/u([0-9a-f]{4})/', "&#x\\1;", $string), ENT_NOQUOTES, 'UTF-8'));
    }
    /**
     * @param $mode
     * @return null|string
     */
    private function getFormatByMode( $mode ) {
        $from = array();
        $to = array();
        $from['date'] = array("MM","M","mm","yyyy","yy","dd");
        $to['date'] = array("m","m","m","Y","Y","d");
        $from['time'] = array("HH","mm");
        $to['time'] = array("H","i");
        $dateFormat = str_replace($from['date'],$to['date'],Yii::app()->locale->getDateFormat('short'));
        $timeFormat = str_replace($from['time'],$to['time'],Yii::app()->locale->getTimeFormat('short'));
        switch ( $mode ) {
            case 'date':
                    $this->to = ($this->to)?$this->to:date( $dateFormat );
                    return $this->to;
                    break;
            case 'range':
                    $this->to   = ($this->to)?$this->to:date( $dateFormat );
                    $this->from = ($this->from)?$this->from:date( $dateFormat );
                    return $this->from.' - '.$this->to;
                    break;
            case 'datetime':
                    $this->to   = ($this->to)?$this->to:date($dateFormat.' '.$timeFormat);
                    return $this->to;
                    break;
        }
        return false;
    }

    /**
     *
     */
    public function run() {

        if (!substr(parent::getId(), -1)) {
            $this->registerExternals();
        }

        if ($this->field != ''){
            if ( $this->nl ) {
                echo "<br/>";
            }
            if ( $this->label != '' ) {
                echo Chtml::label($this->label,$this->field);
            }
            echo CHtml::textField(
                $this->field, $this->getFormatByMode( $this->mode ),
                array(
                    'class' => ''.$this->mode.'picker',
                    'data-ref' => $this->store.'|'.$this->controller->id.'.'.$this->controller->action->id.'|'.$this->zone.$this->alt,
                    'readonly'=>'true'
                )
            );
        }
    }

    /**
     *
     */
    public function registerExternals()
    {
        /** @var $cs CClientScript */
        $cs = Yii::app()->getClientScript();
        $assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets');
        $cs->registerCssFile($assets . '/css/datePicker.css');
        $cs->registerScriptFile($assets . '/js/datePicker.base.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile($assets . '/js/datePicker.js', CClientScript::POS_HEAD);
            $range_names = json_encode(
                array(
                    Yii::t('datePicker', 'Apply'),
                    Yii::t('datePicker', 'Clear'),
                    Yii::t('datePicker', 'From'),
                    Yii::t('datePicker', 'To'),
                    Yii::t('datePicker', 'Custom Range'),
                    Yii::t('datePicker', 'Last 7 Days'),
                    Yii::t('datePicker', 'Last 30 Days'),
                    Yii::t('datePicker', 'This Month'),
                    Yii::t('datePicker', 'Last Month')
                )
            );
            $day_names = json_encode(
                array(
                    Yii::t('datePicker', 'Su'),
                    Yii::t('datePicker', 'Mo'),
                    Yii::t('datePicker', 'Tu'),
                    Yii::t('datePicker', 'We'),
                    Yii::t('datePicker', 'Th'),
                    Yii::t('datePicker', 'Fr'),
                    Yii::t('datePicker', 'Sa')
                )
            );
            $descriptions = json_encode(
                array(
                    Yii::t('datePicker', 'Hour'),
                    Yii::t('datePicker', 'Minute'),
                    Yii::t('datePicker', 'Time Zone'),
                    Yii::t('datePicker', 'Now'),
                    Yii::t('datePicker', 'Done')
                )
            );
            $month_names = json_encode(
                array(
                    Yii::t('datePicker', 'January'),
                    Yii::t('datePicker', 'February'),
                    Yii::t('datePicker', 'March'),
                    Yii::t('datePicker', 'April'),
                    Yii::t('datePicker', 'May'),
                    Yii::t('datePicker', 'June'),
                    Yii::t('datePicker', 'July'),
                    Yii::t('datePicker', 'August'),
                    Yii::t('datePicker', 'September'),
                    Yii::t('datePicker', 'October'),
                    Yii::t('datePicker', 'November'),
                    Yii::t('datePicker', 'December')
                )
            );
            $from = array();
            $to = array();
            $from['date'] = array("MM", "M", "yyyy", "yy", "d", "dddd");
            $to['date'] = array("mm", "mm", "yy", "yy", "dd", "dd");
            $to['range'] = array("M", "MM", "yy", "yyyy", "dd", "dd");
            $from['time'] = array("h", "mm", " a");
            $to['time'] = array("hh", "mm", " tt");
            $dateFormat = str_replace($from['date'], $to['date'], Yii::app()->locale->getDateFormat('short'));
            $rangeFormat = str_replace($from['date'], $to['range'], Yii::app()->locale->getDateFormat('short'));
            $timeFormat = str_replace($from['time'], $to['time'], Yii::app()->locale->getTimeFormat('short'));
            echo CHtml::hiddenField('df', $dateFormat, array('id' => 'dateFormat'));
            echo CHtml::hiddenField('rf', $rangeFormat, array('id' => 'rangeFormat'));
            echo CHtml::hiddenField('tf', $timeFormat, array('id' => 'timeFormat'));
            echo CHtml::hiddenField('rn', $this->unicodeToUtf($range_names), array('id' => 'rangeNames'));
            echo CHtml::hiddenField('mn', $this->unicodeToUtf($month_names), array('id' => 'monthNames'));
            echo CHtml::hiddenField('dn', $this->unicodeToUtf($day_names), array('id' => 'dayNames'));
            echo CHtml::hiddenField('dn', $this->unicodeToUtf($descriptions), array('id' => 'descriptions'));
    }

}
?>
