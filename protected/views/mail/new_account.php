<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 03.06.13
 * Time: 18:10
 */
?>
<html>
    <head>
    </head>
    <body>
    &nbsp;&nbsp;&nbsp;Witaj <?php echo($data['f_name'].' '.$data['l_name']); ?>.<br/>
    <br/>
    Dziękujemy za założenie konta w serwisie 'Konkursy Facebook'.<br/>
    Aby dokończyć rejestrację prosimy i kliknięcie w poniższy link:<br/><br/>
    <a href="<?php echo($url['ok']); ?>" target="_blank"><?php echo($url['ok']); ?></a><br/><br/>
    Jeżeli uważasz, że ten mail nie powinien do Ciebie trafić, lub nie chcesz konta w naszym systemie prosimy o skorzystanie z poniższego linku:<br/>
    <a href="<?php echo($url['no']); ?>" target="_blank"><?php echo($url['no']); ?></a><br/><br/>
    __<br/>
    Konkursy Facebook<br/>
    Grzegorz Wilczyński<br/>
    EWSIE (854)
    </body>
</html>