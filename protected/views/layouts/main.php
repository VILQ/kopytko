<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="pl" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/modals.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="fb-root"></div>
<script type="text/javascript">
    FB.init({
        appId  : '421551651265443',
        status : true, // check login status
        cookie : true, // enable cookies to allow the server to access the session
        xfbml  : true, // parse XFBML
        channelUrl : '/channel.php', // channel.html file
        oauth  : true // enable OAuth 2.0
    });
</script>

<div class="container" id="page">

	<div id="header">
		<div id="logo"></div>
	</div><!-- header -->

	<div id="mainMenu">
        Konkursy Facebook <span id="breadcrumb"></span>
        <div class="menuItem" id="logout"></div>
        <div class="menuItem" id="settings"></div>
        <?php if(empty(Yii::app()->session['avatar'])): ?>
            <div id="avatar2" class="FBConnect"></div>
        <?php else: ?>
            <div id="username"><?php echo(Yii::app()->session['username']); ?></div>
            <div id="avatar"> <img src="<?php echo(Yii::app()->session['avatar']); ?>"></div>
        <? endif; ?>
	</div><!-- mainmenu -->

    <div id="center">
        <div id="leftMenu" class="left">
            <div id="dashboard" class="leftMenuItem active" data-href="">
                <div class="logo"></div>
                <div class="desc">Dashboard</div>
            </div>
            <div id="new_app" class="leftMenuItem" data-href="newapp">
                <div class="logo"></div>
                <div class="desc">Nowa aplikacja</div>
            </div>
        </div>
        <?php echo $content; ?>
    </div>
</div><!-- page -->
<div class="modal" id="error">
    <div class="modal-body-single">
        <div id="errorMsg"></div>
        <span class="btn2" id="errorOK">OK</span>
    </div>
</div>
<div class="modal-backdrop"></div>
<div class="modal-backdrop-locked"><div class="modal-spinner"></div></div>
</body>
</html>
