<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/app'); ?>
    <div id="contents">
        <?php echo $content; ?>
    </div><!-- content -->
    <div class="modal" id="error">
        <div class="modal-body-single">
            <div id="errorMsg"></div>
            <span class="btn2" id="errorOK">OK</span>
        </div>
    </div>
    <div id="spinner"><div id="spinnerImg"></div></div>
    <div id="footer">
        Ta aplikacja nie jest sponsorowana, prowadzona administrowana i związana z Facebookiem w żaden sposób. Jako uczestnik rozumiesz, że dostarczasz informacje <br/>organizatorowi promocji, a nie Facebookowi. Ponadto, biorąc udział w Konkursie zgadzasz się na waruntki zawarte w <?php echo(CHtml::link('regulaminie',$this->terms_url,array('target'=>'_blank'))); ?>.
    </div>
    <div class="modal-backdrop"></div>
    <div class="modal-backdrop-locked"><div class="modal-spinner"></div></div>
<?php $this->endContent(); ?>