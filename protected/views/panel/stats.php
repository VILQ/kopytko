<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 13.06.13
 * Time: 23:15
 */
?>
<div class="backButton2 cButton" data-href="stats"><< porwót</div>
<div id="statsHolder">
<style type="text/css">
    .chart {
        padding: 50px;
    }
    #statsHeader {
        text-align: center;
        font-size: 16px;
    }
    #statsHolder {
        padding-top: 20px;
    }
    .statsApp {
        font-weight: bold;
    }
    .comment {
        padding: 0px 50px;
        font-size: 14px;
        line-height: 1.5em;
        text-align: justify;
        width: 66%;
        margin: 0 auto;
    }
    #exportStatsToPdf {
        width: 128px;
        height: 128px;
        background: url('/images/pdf.png');
        cursor:pointer;
        margin: 30px auto;
    }
    #exportStatsToPdf:hover {
        filter: alpha(opacity=50);
        opacity: 0.5;
    }
</style>
    <div id="statsHeader">
        Statystyki aplikacji <span class="statsApp"><?php echo($app->name); ?></span> na fanpage'u <span class="statsApp"><?php echo($fp->name); ?></span>
    </div>
    <?php foreach($stats as $name=>$stat): ?>
        <div class="chart" id="chart<?php echo(ucfirst($name)); ?>"></div>
        <div class="comment" id="c_<?php echo(ucfirst($name));?>">
            <?php
                    switch($name){
                        case 'users':
                            $first = current($stat['sum']);
                            $last = end($stat['sum']);
                            echo 'W pierwszym dniu funkcjonowania aplikacji zarejestrowało się <span class="statsApp">'.$first.'</span> nowych użytkowników, a najwyższy przyrost (&nbsp;o&nbsp;<span class="statsApp">'.max($stat['growth']).'</span>&nbsp;) przypada na dzień <span class="statsApp">'.array_search(max($stat['growth']),$stat['growth']).'</span>. Ilość głosów w ostanim dniu aktualnego okresu wynosi: <span class="statsApp">'.$last.'</span>. Całkowity przyrost użytkowników od pierwszego dnia wynosi: <span class="statsApp">'.($last - $first).'</span>.';
                            break;
                        case 'answers':
                            $first = current($stat['sum']);
                            $last = end($stat['sum']);
                            echo 'W pierwszym dniu funkcjonowania aplikacji wpłynęło&nbsp;<span class="statsApp">'.$first.'</span> zgłoszeń, a najwyższy przyrost (&nbsp;o&nbsp;<span class="statsApp">'.max($stat['growth']).'</span>&nbsp;) przypada na dzień <span class="statsApp">'.array_search(max($stat['growth']),$stat['growth']).'</span>. Ilość głosów w ostanim dniu aktualnego okresu wynosi: <span class="statsApp">'.$last.'</span>. Całkowity przyrost zgłoszeń od pierwszego dnia wynosi: <span class="statsApp">'.($last - $first).'</span>.';
                            break;
                        case 'hourly':
                            $first = array_search(max($stat['growth']),$stat['growth']);
                            $first_val = max($stat['growth']);
                            unset($stat['growth'][$first]);
                            $second = array_search(max($stat['growth']),$stat['growth']);
                            $second_val = max($stat['growth']);
                            unset($stat['growth'][$second]);
                            $third = array_search(max($stat['growth']),$stat['growth']);
                            $third_val = max($stat['growth']);
                            echo 'Największą popularnością aplikacja cieszy się między godziną <span class="statsApp">'.$first.'</span> a <span class="statsApp">'.str_replace(':00',':59',$first).'</span> i wynosi: <span class="statsApp">'.$first_val.'</span>. Drugi w kolejności jest przedział między <span class="statsApp">'.$second.'</span> a <span class="statsApp">'.str_replace(':00',':59',$second).'</span> z wynikiem: <span class="statsApp">'.$second_val.'</span>, a trzeci między <span class="statsApp">'.$third.'</span> a <span class="statsApp">'.str_replace(':00',':59',$third).'</span> z wynikiem: <span class="statsApp">'.$third_val.'</span>.';
                            break;
                        case 'likes':
                            $first = current($stat['sum']);
                            $last = end($stat['sum']);
                            echo 'W pierwszym dniu funkcjonowania aplikacji fanpage <span class="statsApp">'.$fp->name.'</span> posiadał <span class="statsApp">'.$first.'</span> fanów, a najwyższy przyrost (&nbsp;o&nbsp;<span class="statsApp">'.max($stat['growth']).'</span>&nbsp;) przypada na dzień <span class="statsApp">'.array_search(max($stat['growth']),$stat['growth']).'</span>. Ilość fanów w ostanim dniu aktualnego okresu wynosi: <span class="statsApp">'.$last.'</span>. Całkowity przyrost fanów od pierwszego dnia wynosi: <span class="statsApp">'.($last - $first).'</span>.';
                            break;
                        default:
                    }
            ?>
        </div>
    <?php endforeach; ?>
    </div>
</div>
<div id="exportStatsToPdf"></div>
<form method="post" target="_blank" action="/pdf" id="pdfExportForm">
    <input type="hidden" name="css" id="f_css"/>
    <input type="hidden" name="header" id="f_header"/>
    <input type="hidden" name="chart1" id="chart1"/>
    <input type="hidden" name="chart2" id="chart2"/>
    <input type="hidden" name="chart3" id="chart3"/>
    <input type="hidden" name="chart4" id="chart4"/>
    <input type="hidden" name="comment1" id="comment1"/>
    <input type="hidden" name="comment2" id="comment2"/>
    <input type="hidden" name="comment3" id="comment3"/>
    <input type="hidden" name="comment4" id="comment4"/>
    <input type="hidden" name="app_name" value="<?php echo($app->name); ?>"/>
    <input type="hidden" name="fb_name" value="<?php echo($fp->name); ?>"/>
    <input type="hidden" name="start" value="<?php echo(current(explode(" ",$this->dateFormat($app->date_start)))); ?>"/>
    <input type="hidden" name="stop" value="<?php echo($app->date_end < date("Y-m-d H:i:s"))?$this->dateFormat($app->date_end):date("d.m.Y"); ?>"/>
</form>
<script type="text/javascript">
    Highcharts.setOptions({lang: {resetZoom: 'Reset zoom',resetZoomTitle: 'Reset zoom (1:1)'}});
    $(function () {
        <?php foreach($stats as $name=>$array): ?>
        $("#chart<?php echo(ucfirst($name)); ?>").highcharts({legend:{enabled:1},credits:{enabled:1,text:"konkursyfb.vilq.com",href:"http://konkursyfb.vilq.com",position:{align:"right",x:-5,verticalAlign:"bottom",y:-5}},exporting:{enabled:0},labels:{rotation:-90,style:{fontSize:"11px"},align:"right"},chart:{type:"spline",marginTop:50,marginRight:25,marginBottom:140,marginLeft:70},yAxis:{minRange:.5,title:{text:""}},title:{text:"<?php echo($array['title']); ?>"},xAxis:{labels:{rotation:-90,style:{fontSize:"11px"},align:"right"},categories:[<?php echo $this->arrayToSeries( array_keys($array['growth']) ); ?>],minRange:.5},series:[{name:"<?php echo($array['column']); ?>",type:"column",data:[<?php echo(implode(",",array_values($array['growth']))); ?>]}<?php if( isset($array['spline']) ):?>,{name:"<?php echo($array['spline']); ?>",type:"spline",data:[<?php echo(implode(",",array_values($array['sum']))); ?>]}<?php endif; ?>]});
        <?php endforeach; ?>
    });
</script>