<?php
/* @var $this PanelController */
$count = 3;

    if ($this->msg != '') {
        echo '<div class="infoMsg">'.$this->msg.'</div>';
        $this->msg = '';
    }
?>
<div id="myDashboard" style="width:<?php echo $count*250;?>px;">
    <div class="dashboard" data-id="myapps" id="myapps">
        <div class="logo"></div>
        <div class="description">Moje aplikacje</div>
    </div>
    <div class="dashboard" data-id="stats" id="stats">
        <div class="logo"></div>
        <div class="description">Statystyki</div>
    </div>
    <div class="dashboard" data-id="archive" id="archive">
        <div class="logo"></div>
        <div class="description">Archiwum</div>
    </div>

</div>
<script type="text/javascript">
    $(function(){
        $("#breadcrumb").html('(Dashboard)');
    });
</script>