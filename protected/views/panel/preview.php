<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 15.06.13
 * Time: 23:02
 */
function aasort(&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
}
$view = unserialize($app->view);
$rewards = unserialize($app->rewards);
$questions = unserialize($app->questions);
$answers = unserialize($app->answers);
if (isset($answers['hidden']) && $answers['hidden'] == 1){
    $hidden = true;
}
aasort($rewards,'place');
$first_menu = 'Konkurs';
switch ( $app->type ) {
    case 'text':
        $desc = 'Odpowiedzi';
        break;
    case 'photo':
        $desc = 'Zgłoszenia';
        break;
    case 'quiz':
        $desc = 'Wyniki';
        $first_menu = 'Quiz';
        break;
    default:
        $desc = '&nbsp;';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="pl" />

    <title><?php echo CHtml::encode($app->name.' (podlgąd)'); ?></title>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/preview.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/preview.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/modals.css" />
    <style type="text/css">
        <?php echo $view['css']; ?>
    </style>
</head>

<body>
    <div id="topMenuHolder">
        <div id="topmenu">
            <div class="cButton active" data-tab="welcome">Ekran powitalny</div>
            <div class="cButton" id="appButton" data-tab="app">Ekran konkursu/aplikacji</div>
            <div class="cButton" data-tab="terms">Ekran zasad</div>
            <div class="cButton" data-tab="rewards">Ekran nagród</div>
            <?php if (!$hidden): ?>
                <div class="cButton" data-tab="scores">Ekran wyników/zgłoszeń</div>
            <?php endif; ?>
        </div>
    </div>
    <div id="p_contents">
        <div id="header"><?php echo($app->name); ?></div>
        <div id="menu">
            <div class="menuItem first active" id="mapp"><?php echo($first_menu); ?></div>
            <div class="menuItem" id="mterms">Zasady</div>
            <div class="menuItem<?php echo($hidden)?' last':''; ?>" id="mrewards">Nagrody</div>
            <?php if (!$hidden): ?>
                <div class="menuItem last" id="mscores"><?php echo($desc); ?></div>
            <?php endif; ?>
        </div>
        <?php if ( !empty($view['header'])): ?>
            <div id="appHeader"><img src="/<?php echo($view['header']); ?>"/></div>
        <?php endif; ?>
        <div id="appBody">
            <div class="screens" id="welcome">
                <div id="infoText">
                    <?php echo(urldecode($app->info)); ?>
                </div>
                <div class="terms">
                    <span id="proceed" class="btn2">
                        Weź udział
                    </span><br/>
                    <input type="checkbox" id="term" name="terms"><label for="terms">Oświadczam, iż zapoznałem się z <?php echo(CHtml::link('regulaminem',$view['terms_url'],array('target'=>'_blank'))); ?> i w pełni go akceptuję.</label>
                </div>
            </div>
            <div class="screens" id="app">
                <div id="questions">
                    <?php
                        switch ( $app->type ) {
                            case 'quiz':
                                $q = current($questions);
                                $output = '<div class="qHolder" id="h1" style="display:block;">';
                                $output .= '<div class="qQuestion" id="q1">'.$q['q'].'</div>';
                                foreach ( $q['a']  as $a=>$answer ){
                                    $output .= '<div class="qAnswer" id="a1|'.$a.'"><input type="radio" name="q1" value="'.$a.'"/><label for="1|'.$a.'">'.$answer.'</label></div>';
                                }
                                $output .= '<span class="nextQuestion" id="n2" data-q="q1">Dalej</span>';
                                $output .= '</div>';
                                break;
                            case 'text':
                                switch($questions['texttype']){
                                    case 's':
                                        $output .= '<div id="quizHolder"><div class="qHolder" id="qsh"><span class="btn2">START</span></div>';
                                        $output .= '<div class="qHolder" id="h1">';
                                        $output .= '<select id="qSelect"><option value="0" DISABLED SELECTED>-- wybierz z listy --</option>';
                                        foreach ( $questions as $id=>$question ) {
                                            if ($id == 'texttype' ) continue;
                                            $output .= '<option value="q'.$id.'">'.$question['q'].'</option>';
                                        }
                                        $output .= '</select>';
                                        $output .= '<textarea name="q1" id="text1" class="cArea"/></textarea>';
                                        $output .= '<span class="nextQuestion finish" id="n2" data-q="0">Wyślij</span></div>';
                                        $output .= '<div class="qHolder finish" id="h2">Dziękujemy za wzięcie udziału w konkursie.</div>';
                                        $output .= '</div>';
                                        break;
                                    case 'm':
                                    default:
                                        $output .= '<div id="quizHolder"><div class="qHolder" id="qsh"><span class="btn2">START</span></div>';
                                        $cur = 0;
                                        foreach ( $questions as $id=>$question ) {
                                            if ($id == 'texttype' ) continue;
                                            $output .= '<div class="qHolder" id="h'.++$cur.'">';
                                            $output .= '<div class="qQuestion text" id="q'.$id.'">'.$question['q'].'</div>';
                                            $output .= '<textarea name="q'.$id.'" id="text'.$id.'" class="cArea"/></textarea>';
                                            $output .= '<span class="nextQuestion" id="n'.($cur+1).'" data-q="q'.$id.'">Dalej</span>';
                                            $output .= '</div>';
                                        }
                                        $output .= '<div class="qHolder finish" id="h'.($cur+1).'">Dziękujemy za wzięcie udziału w konkursie.</div>';
                                        $output .= '</div>';
                                }
                                break;
                            default:
                        }
                        echo $output;
                    ?>
                </div>
            </div>
            <div class="screens" id="terms">
                <?php echo urldecode($app->terms); ?>
            </div>
            <div class="screens" id="rewards">
                <div id="rewardsHolder">
                    <?php foreach ($rewards as $reward): ?>
                        <div class="reward">
                            <div class="rewardImg">
                                <img src="/<?php echo($reward['photo']); ?>"/>
                                <div class="rewardPlace">
                                    <?php echo($reward['place']); ?>
                                </div>
                            </div>
                            <div class="rewardName">
                                <?php echo($reward['name']); ?>
                            </div>
                            <div class="rewardDesc">
                                <?php echo($reward['desc']); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="screens" id="scores">
                <div id="scoresHolder">
                    <?php
                        switch($app->type){
                                case 'quiz':
                                     echo '
                                        <div class="scoreLine">
                                            <div class="scoreLp">&nbsp;</div>
                                            <div class="scoreName">Imię i nazwisko</div>
                                            <div class="scoreScore">Wynik</div>
                                            <div class="scoreTime">Czas</div>
                                        </div>';
                                        $answer = array('Imię','Nazwisko','0','00:00,00');
                                        for( $i = 1; $i <= 10; $i++ ){
                                            echo '<div class="scoreLine">';
                                            echo '<div class="scoreLp">'.$i.'</div>';
                                            echo '<div class="scoreName">'.$answer[0].' '.$answer[1].'</div>';
                                            echo '<div class="scoreScore">'.$answer[2].'</div>';
                                            echo '<div class="scoreTime">'.$answer[3].'</div>';
                                            echo '</div>';
                                        }
                                    break;
                                case 'text':
                                    $answer = array('Imię','Nazwisko','przykładowa odpowiedź na pytanie');
                                    for( $i = 1; $i < count($questions); $i++ ){
                                        echo '<div class="scoreLine">';
                                        echo '<div class="scoreName">'.$answer[0].' '.$answer[1].'</div>';
                                        switch ( $questions['texttype'] ){
                                            case 's':
                                                $dataContent = urlencode('<div class="modalQuestion">'.$questions[$i]['q'].'</div><div class="modalAnswer">'.$answer[2].'</div>');
                                                break;
                                            case 'm':
                                            default:
                                                $tmp = '';
                                                foreach ($questions as $id=>$question){
                                                    if ($id == 'texttype') continue;
                                                    $tmp .= '<div class="modalQuestion">'.$question['q'].'</div><div class="modalAnswer">'.$answer[2].'</div>';
                                                }
                                                $dataContent = urlencode($tmp);
                                        }
                                        echo '<div class="scoreScore withDate"><span class="show_story" data-content="'.$dataContent.'"><img src="/images/textedit_16.png"/></div>';
                                        echo '<div class="scoreTime withDate">'.date("d.m.Y H:i").'</div>';
                                        echo '</div>';
                                    }
                                break;
                            default:
                                echo 'brak wpisów';
                        }
                        ?>
                </div>
            </div>
        </div>
        <div id="spinner"><div id="spinnerImg"></div></div>
        <div id="footer">
            Ta aplikacja nie jest sponsorowana, prowadzona administrowana i związana z Facebookiem w żaden sposób. Jako uczestnik rozumiesz, że dostarczasz informacje <br/>organizatorowi promocji, a nie Facebookowi. Ponadto, biorąc udział w Konkursie zgadzasz się na waruntki zawarte w <?php echo(CHtml::link('regulaminie',$view['terms_url'],array('target'=>'_blank'))); ?>.
        </div>
        <div class="modal" id="error">
            <div class="modal-body-single">
                <div id="errorMsg"></div>
                <span class="btn2" id="errorOK">OK</span>
            </div>
        </div>
        <div class="modal-backdrop"></div>
        <div class="modal-backdrop-locked"><div class="modal-spinner"></div></div>
    </div>
</body>
</html>