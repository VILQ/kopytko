<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 04.06.13
 * Time: 14:21
 */
$view = $this->prepareEdit( $app,'view');
$questions = $this->prepareEdit( $app,'questions');
$answers = $this->prepareEdit( $app,'answers');
$rewards = $this->prepareEdit( $app,'rewards');
?>

<div id="newApp">
    <div id="newAppMenu">
        <span id="nAG" data-tab="newAppGeneral" class="active">Ogólne</span>
        <span id="nAV" data-tab="newAppVisual">Wygląd</span>
        <span id="nAT" data-tab="newAppTerms">Zasady</span>
        <span id="nAQ" data-tab="newAppQuestion">Pytania</span>
        <span id="nAR" data-tab="newAppRewards">Nagrody</span>
        <?php if ($app->data!="temp"):?><span id="nAPa" data-tab="newAppParticipants">Uczestnicy</span><?php endif; ?>
        <span id="nAP" data-tab="newAppPublish" class="last">Zapis / Usunięcie</span>
    </div>
    <div id="newAppBody">
        <form method="post" action="/panel/create" id="newAppForm">
            <div id="newAppGeneral" class="newAppTabs">
                <input class="cInput" type="text" placeholder="Nazwa aplikacji" name="appName" id="appName" value="<?php echo($app->name); ?>"/>
                <input type="hidden" name="preview" id="preview"/>
                <?php
                Yii::import('application.extensions.datePicker.datePicker');
                $app_start = new DateTime($app->date_start);
                $app_end = new DateTime($app->date_end);
                $this->widget('datePicker',array(
                    'label' => 'Początek publikacji:',
                    'mode' => 'datetime',
                    'field' => $app->type.'_start',
                    'nl' => false,
                    'to' => $app_start->format('d.m.Y H:i')
                ));
                $this->widget('datePicker',array(
                    'label' => 'Koniec publikacji:',
                    'mode' => 'datetime',
                    'field' => $app->type.'_end',
                    'nl' => true,
                    'to' => $app_end->format('d.m.Y H:i')
                ));
                ?><br/>
                <hr/>
                <label for="sandbox">Tylko dla moderatorów:</label><input type="checkbox" name="sandbox" id="sandbox" <?php echo($app->sandbox)?'checked="checked':''; ?>/>
                <br/>
                <hr/>
                <div id="limits">
                    <span class="welcome">Limit zgłoszeń</span><br/>
                    <input type="radio" name="view[limit][t]" value="n" <?php echo($view['limit']['t']=='n')?'checked="checked"':''; ?>/>Brak<br/>
                    <input type="radio" name="view[limit][t]" value="i" <?php echo($view['limit']['t']=='i')?'checked="checked"':''; ?>/>Kolejne zgłoszenie po zaproszeniu <span id="minimum" <?php echo(isset($view['limit']['v']) && $view['limit']['t']=='i')?'style="display:inline-block;"':''; ?>>min.<input name="view[limit][v]" class="qScore" value="<?php echo(isset($view['limit']['v']))?$view['limit']['v']:'3'; ?>"/></span>znajomych<br/>
                    <input type="radio" name="view[limit][t]" value="d" <?php echo($view['limit']['t']=='d')?'checked="checked"':''; ?>/>Jedno zgłoszenie na 24h<br/>
                    <input type="radio" name="view[limit][t]" value="c" <?php echo($view['limit']['t']=='c')?'checked="checked"':''; ?>/>Jedno zgłoszenie na konkurs<br/>
                </div>
                <br/>
                <hr/>
                <label for="appLike">Konieczność polubienia strony:</label><input type="checkbox" name="appLike" id="appLike" <?php echo($app->like)?'checked="checked"':''; ?>/>
                <br/>
                <?php if (empty($app->fanpage)): ?>
                    Wybierz Fanpage:
                    <select name="appFp" id="appFp" class="cInput">
                        <?php
                        $pages = current($user)->pages;
                        if ( empty($pages) ) {
                            echo '<option disabled>Podłącz konto FB lub dodaj Fanpage</option>';
                        } else {
                            echo '<option value="0" disabled selected>-- wybierz z listy --</option>';
                            foreach ($pages as $page){
                                if (in_array($page->fb_id,$check)) {
                                    continue;
                                }
                                echo '<option value="'.$page->fb_id.'">'.$page->name.'</option>';
                            }
                        }
                        ?>
                    </select>
                <?php else: ?>
                    Fanpage:<span id="quizFanpage">
                    <?php
                        $pages = current($user)->pages;
                        foreach ($pages as $page){
                            if ($app->fanpage==$page->fb_id) echo $page->name;
                        }
                    ?></span>
                <?php endif; ?>
                <br/>
                <br/>
                <hr/>
                <br/>
                <label>Link do regulaminu: </label><input type="text" class="cInput" id="termsUrl" placeholder="Podaj adres URL (http://***)" name="view[terms_url]" value="<?php echo($view['terms_url']); ?>"/>
                <br/>
                <input type="hidden" name="appFp" id="appFp" value="<?php echo($app->fanpage); ?>"
                <input type="hidden" name="app_id" id="app_id" value="<?php echo($app->app_id); ?>"/>
                <input type="hidden" name="id" value="<?php echo($app->id); ?>"/>
                <input type="hidden" id="contestType" name="type" value="<?php echo($app->type); ?>"/>
                <input type="hidden" name="questions[texttype]" value="<?php echo($questions['texttype']);?>"/>
            </div>
            <div id="newAppVisual" class="newAppTabs">
                <span class="welcome">Obrazek nagłówka:</span><br/>
                <?php
                    $this->createUploader('header','(max. szerokość 790px, max. rozmiar 2MB)','view',($view['header']!=''),$view['header']);
                ?>
                <span class="welcome">Tekst powitalny:</span><br/>
                <?php
                $this->widget('KRichTextEditor', array(
                    'model' => 'info',
                    'name' => 'info',
                    'value' => urldecode($app->info),
                    'attribute' => 'content',
                    'options' => array(
                        'theme_advanced_resizing' => 'false',
                        'theme_advanced_statusbar_location' => 'bottom',
                    ),
                ));
                ?><br/>
                <div class="cButton" id="appPreview">Podgląd</div>
                <span class="cButton" id="adv">Zaawansowana edycja</span><br/>
                <div id="advanced">
                    Dodakowy kod CSS:<br/>
                    <textarea class="cInput" name="view[css]" id="customCSS"><?php echo(empty($view['css'])?'#appBody{width:500px;margin:0 auto;text-align:center;}':$view['css']); ?></textarea>
                </div>
            </div>
            <div id="newAppTerms" class="newAppTabs">
                <span class="welcome">Treść zasad:</span><br/>
                <?php
                    $this->widget('KRichTextEditor', array(
                        'model' => 'terms',
                        'name' => 'terms',
                        'value' => urldecode($app->terms),
                        'attribute' => 'content',
                        'options' => array(
                            'theme_advanced_resizing' => 'false',
                            'theme_advanced_statusbar_location' => 'bottom',
                        ),
                    ));
                ?>
                <br/>
                Zmienne:<br/>
                $START$ - data rozpoczęcia konkursu<br/>
                $STOP$ - data zakończenia konkursu<br/>
            </div>
            <div id="newAppQuestion" class="newAppTabs">
                <span class="welcome">Pytania konkursowe<?php if ($app->data!="temp") { echo($questions['texttype']=='s')?' (Wybór jednego pytania z listy)':' (Odpowiedź na wszystkie pytania)'; }?>:</span><span class="nextQuestionSingle" data-q="<?php echo(count($questions)+1); ?>">+</span><br/>
                <input type="checkbox" name="good[hidden]" value="1" <?php echo($answers['hidden']=="1")?'checked="checked"':'';?>"/>Ukryj zakładkę 'Odpowiedzi'<br/><br/>
                <?php if ($app->data=="temp"):?>
                    <input type="radio" name="questions[texttype]" value="s" <?php echo($questions['texttype']=='s')?'checked="checked"':''; ?>/>Wybór jednego pytania z listy<br/>
                    <input type="radio" name="questions[texttype]" value="m" <?php echo($questions['texttype']=='m')?'checked="checked"':''; ?>/>Odpowiedź na wszystkie pytania<br/><br/>
                <?php endif; ?>
                <?php foreach($questions as $id=>$question): ?>
                    <?php if ( $id == 'texttype') continue; ?>
                    <div class="questions single" id="q<?php echo($id); ?>" data-question="<?php echo($id); ?>">
                        <div class="question"><input class="cInput" type="text" name="questions[<?php echo($id); ?>][q]" placeholder="Pytanie/Temat <?php echo($id); ?>" value="<?php echo($question['q']); ?>" <?php echo($app->data!="temp")?"readonly":"";?>/></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div id="newAppRewards" class="newAppTabs">
                <span class="welcome">Nagrody:</span><span class="cButton" id="addReward">Dodaj nagrodę:</span><br/>
                <?php
                    if (is_array($rewards)){
                        foreach ($rewards as $idx=>$reward) {
                            echo '<div class="newReward" id="new_'.$idx.'">';
                            echo ($idx != 'reward1')?'<hr/>':'';
                            $this->createUploader($idx,'(max. szerokość 250px, max. rozmiar 2MB)','rewards',true,$reward['photo'],'[photo]');
                            echo '<label>Nagroda za miejsce:</label><input type="text" class="qScore pReward" placeholder="#" name="rewards['.$idx.'][place]"    value="'.$reward['place'].'"/><br/>
                            <label>Nazwa nagrody:</label><input type="text" class="cInput qReward" placeholder="Nazwa nagrody" name="rewards['.$idx.'][name]"     value="'.$reward['name'].'"/>
                            <label>Krótki opis:</label><input type="text" class="cInput qReward" placeholder="Opis nagrody" name="rewards['.$idx.'][desc]" value="'.$reward['desc'].'"/>
                        </div>';
                        }
                    }
                ?>
            </div>
            <?php if ($app->data!="temp"):?>
            <div id="newAppParticipants" class="newAppTabs">
                <span class="welcome">Zgłoszenia:</span>
                <div class="scoreLine">
                    <div class="scoreLp">&nbsp;</div>
                    <div class="scoreName">Imię i nazwisko</div>
                    <div class="scoreScore"></div>
                    <div class="scoreTime">Data:</div>
                </div>
                <?php
                    $lp = 0;
                    foreach ($participants as $part){
                        echo '<div class="scoreLine">';
                        echo '<div class="scoreDelete" data-id="'.$part->id.'"></div>';
                        echo '<div class="scoreLp">'.++$lp.'</div>';
                        echo '<div class="scoreName">'.$part->user->first_name.' '.$part->user->last_name.'</div>';
                        $answer = unserialize($part->sort_string);
                        switch ( $questions['texttype'] ){
                            case 's':
                                $dataContent = urlencode('<div class="modalQuestion">'.$questions[current(array_keys($answer))]['q'].'</div><div class="modalAnswer">'.current($answer).'</div>');
                                break;
                            case 'm':
                            default:
                                $tmp = '';
                                foreach ($questions as $id=>$question){
                                    if ($id == 'texttype') continue;
                                    $tmp .= '<div class="modalQuestion">'.$question['q'].'</div><div class="modalAnswer">'.$answer[$id].'</div>';
                                }
                                $dataContent = urlencode($tmp);
                        }
                        echo '<div class="scoreScore"><span class="show_story" data-content="'.$dataContent.'"><img src="/images/textedit_16.png"/></div>';
                        echo '<div class="scoreTime">'.$this->dateTimeFormat($part->created).'</div>';
                        echo '</div>';
                        echo '<div class="scoreData">';
                        echo CHtml::link($part->user->email,'mailto:'.$part->user->email,array('class'=>'scoreMail'));
                        echo CHtml::link('Profil FB','//facebook.com/'.$part->user->fb_id,array('class'=>'scoreFB', 'target' => '_blank'));
                        echo '</div>';

                    }
                ?>
            </div>
            <?php endif; ?>
            <div id="newAppPublish" class="newAppTabs">
                <input class="cButton" type="submit" value="<?php echo($app->data=="temp")?'Publikacja':'Zapisz zmiany'; ?>" id="appSubmit"/>
                <span class="cButton" id="deleteApp" data-app="<?php echo($app->id); ?>">Przenieś do archiwum</span>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    question = <?php echo(empty($questions)?1:count($questions)); ?>;
    reward = <?php echo(count($rewards)); ?>;
</script>