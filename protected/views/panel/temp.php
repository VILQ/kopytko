<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 16.06.13
 * Time: 12:58
 */
?>

<div id="temp">
    <div id="tempHeader">Wykryto niezapisaną aplikację <span class="welcome"><?php echo(strtoupper($type)); ?></span>.<br/>Czy chcesz kontynować edycję ?</div>
    <div class="cButton" id="appContinue" data-app="<?php echo($app_id); ?>" data-mode="<?php echo($type); ?>">Kontynuuj</div>
    <div class="cButton" id="appDelete" data-app="<?php echo($app_id); ?>" data-mode="<?php echo($type); ?>">Zacznij od nowa</div>
</div>