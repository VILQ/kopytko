<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 04.06.13
 * Time: 10:16
 */
$count = 2;
?>
<div id="newApplication" style="width:<?php echo $count*250;?>px;">
    <div class="application" data-id="quiz" id="quiz">
        <div class="logo"></div>
        <div class="description">Quiz</div>
    </div>
    <div class="application" data-id="text" id="text">
        <div class="logo"></div>
        <div class="description">Tekstowy</div>
    </div>
    <div class="application" data-id="photo" id="photo" style="display:none;">
        <div class="logo"></div>
        <div class="description">Foto</div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $(".leftMenuItem").removeClass('active');
        $("#new_app").addClass('active');
        $("#breadcrumb").html('(Nowa aplikacja)');
    });
</script>