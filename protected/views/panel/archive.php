<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 17.06.13
 * Time: 00:22
 */
$archive = array(
    'Nazwa aplikacji' => $app->name,
    'Fanpage' => $fp->name,
    'Typ aplikacji' => ucfirst($app->type),
    'Data rozpoczęcia' => $this->dateTimeFormat($app->date_start),
    'Data zakończenia' => $this->dateTimeFormat($app->date_end),
    'Ilość użytkowników' => count($app->parts),
    'Ilość zgłoszeń' => count($app->answers0),
    '<hr/>' => ''
);
$questions = $this->prepareEdit( $app,'questions');
$answers = $this->prepareEdit( $app,'answers');
$rewards = $this->prepareEdit( $app,'rewards');
?>
<div class="backButton2 cButton" data-href="archive"><< porwót</div>
<div id="archiveHolder">
    <?php foreach($archive as $name=>$value): ?>
        <div class="archiveStatRow">
            <div class="archiveStatParam">
                <?php echo($name); ?>
            </div>
            <div class="archiveStatValue">
                <?php echo($value); ?>
            </div>
        </div>
    <?php endforeach; ?>
    <span class="welcome">Zgłoszenia:</span>
    <?php
        switch ($app->type){
            case 'text':
                echo '
                    <div class="scoreLine">
                        <div class="scoreLp">&nbsp;</div>
                        <div class="scoreName">Imię i nazwisko</div>
                        <div class="scoreScore"></div>
                        <div class="scoreTime">Data:</div>
                    </div>';
                $lp = 0;
                foreach ($participants as $part){
                    echo '<div class="scoreLine">';
                    echo '<div class="scoreLp">'.++$lp.'</div>';
                    echo '<div class="scoreName">'.$part->user->first_name.' '.$part->user->last_name.'</div>';
                    $answer = unserialize($part->sort_string);
                    switch ( $questions['texttype'] ){
                        case 's':
                            $dataContent = urlencode('<div class="modalQuestion">'.$questions[current(array_keys($answer))]['q'].'</div><div class="modalAnswer">'.current($answer).'</div>');
                            break;
                        case 'm':
                        default:
                            $tmp = '';
                            foreach ($questions as $id=>$question){
                                if ($id == 'texttype') continue;
                                $tmp .= '<div class="modalQuestion">'.$question['q'].'</div><div class="modalAnswer">'.$answer[$id].'</div>';
                            }
                            $dataContent = urlencode($tmp);
                    }
                    echo '<div class="scoreScore"><span class="show_story" data-content="'.$dataContent.'"><img src="/images/textedit_16.png"/></div>';
                    echo '<div class="scoreTime">'.$this->dateTimeFormat($part->created).'</div>';
                    echo '</div>';
                    echo '<div class="scoreData">';
                    echo CHtml::link($part->user->email,'mailto:'.$part->user->email,array('class'=>'scoreMail'));
                    echo CHtml::link('Profil FB','//facebook.com/'.$part->user->fb_id,array('class'=>'scoreFB', 'target' => '_blank'));
                    echo '</div>';

                }
                break;
            case 'quiz':
                echo '
                <div class="scoreLine">
                    <div class="scoreLp">&nbsp;</div>
                    <div class="scoreName">Imię i nazwisko</div>
                    <div class="scoreScore">Wynik</div>
                    <div class="scoreTime">Czas</div>
                </div>';
                $lp = 0;
                foreach ($participants as $part){
                    echo '<div class="scoreLine'.(++$lp<=count($rewards)?' scoreReward':'').'">';
                    echo '<div class="scoreDelete" data-id="'.$part->id.'"></div>';
                    echo '<div class="scoreLp">'.$lp.'</div>';
                    echo '<div class="scoreName">'.$part->user->first_name.' '.$part->user->last_name.'</div>';
                    echo '<div class="scoreScore">'.$part->sort_int.'</div>';
                    echo '<div class="scoreTime">'.$part->data.'</div>';
                    echo '</div>';
                    if ($lp<=count($rewards)){
                        echo '<div class="scoreData">';
                        echo CHtml::link($part->user->email,'mailto:'.$part->user->email,array('class'=>'scoreMail'));
                        echo CHtml::link('Profil FB','//facebook.com/'.$part->user->fb_id,array('class'=>'scoreFB', 'target' => '_blank'));
                        echo '</div>';
                    }
                }
                break;
            default:
        }
    ?>
</div>