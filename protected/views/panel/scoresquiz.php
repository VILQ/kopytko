<span class="welcome">Wyniki:</span> (tylko najlepszy dla danego użytkownika, poświetlone miejsca nagrodzone)
<div class="scoreLine">
    <div class="scoreLp">&nbsp;</div>
    <div class="scoreName">Imię i nazwisko</div>
    <div class="scoreScore">Wynik</div>
    <div class="scoreTime">Czas</div>
</div>
<?php
$lp = 0;
foreach ($participants as $part){
    echo '<div class="scoreLine'.(++$lp<=count($rewards)?' scoreReward':'').'">';
    echo '<div class="scoreDelete" data-id="'.$part->id.'"></div>';
    echo '<div class="scoreLp">'.$lp.'</div>';
    echo '<div class="scoreName">'.$part->user->first_name.' '.$part->user->last_name.'</div>';
    echo '<div class="scoreScore">'.$part->sort_int.'</div>';
    echo '<div class="scoreTime">'.$part->data.'</div>';
    echo '</div>';
    if ($lp<=count($rewards)){
        echo '<div class="scoreData">';
        echo CHtml::link($part->user->email,'mailto:'.$part->user->email,array('class'=>'scoreMail'));
        echo CHtml::link('Profil FB','//facebook.com/'.$part->user->fb_id,array('class'=>'scoreFB', 'target' => '_blank'));
        echo '</div>';
    }
}
?>