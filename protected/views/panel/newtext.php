<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 16.06.13
 * Time: 16:21
 */
?>
<script type="text/javascript">
    var question = 1;
    var reward = 1;
</script>
<div id="newApp">
    <div id="newAppMenu">
        <span id="nAG" data-tab="newAppGeneral" class="active">Ogólne</span>
        <span id="nAV" data-tab="newAppVisual">Wygląd</span>
        <span id="nAT" data-tab="newAppTerms">Zasady</span>
        <span id="nAQ" data-tab="newAppQuestion">Pytania</span>
        <span id="nAR" data-tab="newAppRewards">Nagrody</span>
        <span id="nAP" data-tab="newAppPublish" class="last">Publikacja</span>
    </div>
    <div id="newAppBody">
        <form method="post" action="/panel/create" id="newAppForm">
            <div id="newAppGeneral" class="newAppTabs">
                <input class="cInput" type="text" placeholder="Nazwa aplikacji" name="appName" id="appName"/>
                <input type="hidden" name="preview" id="preview"/>
                <?php
                Yii::import('application.extensions.datePicker.datePicker');
                $this->widget('datePicker',array(
                    'label' => 'Początek publikacji:',
                    'mode' => 'datetime',
                    'field' => $app->type.'_start',
                    'nl' => false
                ));
                $this->widget('datePicker',array(
                    'label' => 'Koniec publikacji:',
                    'mode' => 'datetime',
                    'field' => $app->type.'_end',
                    'nl' => true
                ));
                ?><br/>
                <hr/>
                <label for="sandbox">Tylko dla moderatorów:</label><input type="checkbox" name="sandbox" id="sandbox"/>
                <br/>
                <hr/>
                <div id="limits">
                    <span class="welcome">Limit zgłoszeń</span><br/>
                    <input type="radio" name="view[limit][t]" value="n" checked="checked"/>Brak<br/>
                    <input type="radio" name="view[limit][t]" value="i"/>Kolejne zgłoszenie po zaproszeniu <span id="minimum">min.<input name="view[limit][v]" class="qScore" value="3"/></span>znajomych<br/>
                    <input type="radio" name="view[limit][t]" value="d"/>Jedno zgłoszenie na 24h<br/>
                    <input type="radio" name="view[limit][t]" value="c"/>Jedno zgłoszenie na konkurs<br/>
                </div>
                <br/>
                <hr/>
                <label for="appLike">Konieczność polubienia strony:</label><input type="checkbox" name="appLike" id="appLike"/>
                <br/>
                Wybierz Fanpage:
                <select name="appFp" id="appFp" class="cInput">
                    <?php
                    $pages = current($user)->pages;
                    if ( empty($pages) ) {
                        echo '<option disabled>Podłącz konto FB lub dodaj Fanpage</option>';
                    } else {
                        echo '<option value="0" disabled selected>-- wybierz z listy --</option>';
                        foreach ($pages as $page){
                            if (in_array($page->fb_id,$check)) {
                                continue;
                            }
                            echo '<option value="'.$page->fb_id.'">'.$page->name.'</option>';
                        }
                    }
                    ?>
                </select>
                <br/>
                <br/>
                <hr/>
                <br/>
                <label>Link do regulaminu: </label><input type="text" class="cInput" id="termsUrl" placeholder="Podaj adres URL (http://***)" name="view[terms_url]" value="<?php echo($view['terms_url']); ?>"/>
                <br/>
                <input type="hidden" name="app_id" id="app_id" value="<?php echo($app->app_id); ?>"/>
                <input type="hidden" name="id" value="<?php echo($app->id); ?>"/>
                <input type="hidden" id="contestType" name="type" value="<?php echo($app->type); ?>"/>
            </div>
            <div id="newAppVisual" class="newAppTabs">
                <span class="welcome">Obrazek nagłówka:</span><br/>
                <?php
                $this->createUploader('header','(max. szerokość 790px, max. rozmiar 2MB)');
                ?>
                <span class="welcome">Tekst powitalny:</span><br/>
                <?php
                $this->widget('KRichTextEditor', array(
                    'model' => 'info',
                    'name' => 'info',
                    'value' => 'To jest przykładowy tekst zachęcający do udziału w konkursie',
                    'attribute' => 'content',
                    'options' => array(
                        'theme_advanced_resizing' => 'false',
                        'theme_advanced_statusbar_location' => 'bottom',
                    ),
                ));
                ?><br/>
                <div class="cButton" id="appPreview">Podgląd</div>
                <span class="cButton" id="adv">Zaawansowana edycja</span><br/>
                <div id="advanced">
                    Dodakowy kod CSS:<br/>
                    <textarea class="cInput" name="view[css]" id="customCSS">#appBody{width:500px;margin:0 auto;text-align:center;}</textarea>
                </div>
            </div>
            <div id="newAppTerms" class="newAppTabs">
                <span class="welcome">Treść zasad:</span><br/>
                <?php
                $this->widget('KRichTextEditor', array(
                    'model' => 'terms',
                    'name' => 'terms',
                    'value' => 'Konkurs - zasady.<br/>Konkurs trwa od $START$ do $STOP$.<br/>Użytkownik odpowiada na jedno bądź kilka pytań ... etc...',
                    'attribute' => 'content',
                    'options' => array(
                        'theme_advanced_resizing' => 'false',
                        'theme_advanced_statusbar_location' => 'bottom',
                    ),
                ));
                ?>
                <br/>
                Zmienne:<br/>
                $START$ - data rozpoczęcia konkursu<br/>
                $STOP$ - data zakończenia konkursu<br/>
            </div>
            <div id="newAppQuestion" class="newAppTabs">
                <span class="welcome">Pytania konkursowe:</span><span class="nextQuestionSingle" data-q="2">+</span><br/>
                <input type="checkbox" name="good[hidden]" value="1"/>Ukryj zakładkę 'Odpowiedzi'<br/><br/>
                <input type="radio" name="questions[texttype]" value="s" checked="checked"/>Wybór jednego pytania z listy<br/>
                <input type="radio" name="questions[texttype]" value="m"/>Odpowiedź na wszystkie pytania<br/>
                <div class="questions single" id="q1" data-question="1">
                    <div class="question"><input class="cInput" type="text" name="questions[1][q]" placeholder="Pytanie/Temat 1"/></div>
                </div>
            </div>
            <div id="newAppRewards" class="newAppTabs">
                <span class="welcome">Nagrody:</span><span class="cButton" id="addReward">Dodaj nagrodę:</span><br/>
                <div id="appRewardsHolder">
                    <div class="newReward" id="new_reward1">
                        <?php
                        $this->createUploader('reward1','(max. szerokość 250px, max. rozmiar 2MB)','rewards',false,'','[photo]');
                        ?>
                        <label>Nagroda za miejsce:</label><input type="text" class="qScore pReward" placeholder="#" name="rewards[reward1][place]"/><br/>
                        <label>Nazwa nagrody:</label><input type="text" class="cInput qReward" placeholder="Nazwa nagrody" name="rewards[reward1][name]"/>
                        <label>Krótki opis:</label><input type="text" class="cInput qReward" placeholder="Opis nagrody" name="rewards[reward1][desc]"/>
                    </div>
                </div>
            </div>
            <div id="newAppPublish" class="newAppTabs">
                <input class="cButton" type="submit" value="Publikacja" id="appSubmit"/>
            </div>
        </form>
    </div>
</div>