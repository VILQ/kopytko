<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 11.06.13
 * Time: 11:21
 */
if ( !Yii::app()->request->isAjaxRequest ):
?>
    <div id="header"><?php echo($app->name); ?></div>
    <div id="menu">
        <div class="menuItem first active" data-step="home"><?php echo(ucfirst($app->type)); ?></div>
        <div class="menuItem" data-step="terms">Zasady</div>
        <div class="menuItem" data-step="reward">Nagrody</div>
        <div class="menuItem last" data-step="scores"><?php echo($desc); ?></div>
    </div>
    <?php if ( !empty($view['header'])): ?>
        <div id="appHeader"><img src="/<?php echo($view['header']); ?>"/></div>
    <?php endif; ?>
<div id="appBody">
    <?php endif; ?>
    <div id="voted">
        <?php
            switch ($view['limit']['t']){
                case 'i':
                    echo 'Aby wziąć udział ponownie zaproś min. '.$view['limit']['v'].' znajomych.<br/>';
                    echo '<span id="FB_Invite" class="btn2">Zaproś znajomych</span>';
                    break;
                case 'd':
                    echo 'Aby wziąć udział ponownie musisz odczekać min. 24h.';
                    break;
                case 'c':
                    echo 'Limit głosów został osiągnięty.<br/>Zapraszamy ponownie w przyszłości';
                    break;
            }
        ?>
    </div>
    <?php if ( !Yii::app()->request->isAjaxRequest ): ?>
</div>
    <script type="text/javascript">var app = <?php echo (int)$app->id; ?>;</script>
<?php endif; ?>
<script type="text/javascript">
    $(function(){
        $("#FB_Invite").on("click", function(){
            $(".modal-backdrop-locked").hide();
            FB.ui({
                    method: 'apprequests',
                    title: '<?php echo($app->name); ?>',
                    message: u_name+' zaprasza Cię do wzpólnej zabawy.'
                },
                function (response) {
                    $.post('/invited',{u_id:u_id,app_id:<?php echo($app->id); ?>,response:response}, function(data){
                        if (data == 'ok'){
                            window.location.reload();
                        }
                    });
                }
            );
        });
    });
</script>