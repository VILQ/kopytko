<div id="rewardsHolder">
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 11.06.13
 * Time: 12:00
 */
    $rewards = unserialize($app->rewards);
    $this->aasort($rewards,'place');
    foreach ($rewards as $reward): ?>

        <div class="reward">
            <div class="rewardImg">
                <img src="/<?php echo($reward['photo']); ?>"/>
                <div class="rewardPlace">
                    <?php echo($reward['place']); ?>
                </div>
            </div>
            <div class="rewardName">
                <?php echo($reward['name']); ?>
            </div>
            <div class="rewardDesc">
                <?php echo($reward['desc']); ?>
            </div>
        </div>

    <?php endforeach; ?>
</div>