<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 10.06.13
 * Time: 10:08
 */
?>
<div id="header"><?php echo($app->name); ?></div>
<?php if ( !empty($view['header'])): ?>
    <div id="appHeader"><img src="/<?php echo($view['header']); ?>"/></div>
<?php endif; ?>
<div id="appBody">
    <div id="infoText">
        <?php echo(urldecode($app->info)); ?>
    </div>
    <div class="terms">
        <span id="proceed" class="btn2">
            Weź udział
        </span><br/>
        <input type="checkbox" id="terms" name="terms"><label for="terms">Oświadczam, iż zapoznałem się z <?php echo(CHtml::link('regulaminem',$this->terms_url,array('target'=>'_blank'))); ?> i w pełni go akceptuję.</label>
    </div>
</div>
<script type="text/javascript">
    var _liked = <?php echo (int)$liked; ?>;
    var app = <?php echo (int)$app->id; ?>;
    $(function(){
        checkterms();
        FB.Canvas.setAutoGrow();
        $("#proceed").on("click",function(e){
            if ( !$('#terms').is(':checked') ) {
                e.preventDefault();
                $(".modal-backdrop").show();
                $("#errorMsg").html('Aby kontynuować musisz zaakceptować regulamin' );
                FB.Canvas.getPageInfo(function(info){
                    $("#error").css('top',100+info.scrollTop+'px');
                    $("#continue").hide();
                    $("#errorOK").show();
                    $("#error").fadeIn('fast');
                });
                return false;
            }
            FB.getLoginStatus(function(response) {
                if (response.status !== 'connected') {
                    FB.login(function(response) {
                        if (response.authResponse) {
                            FB.api('/me', function(response) {
                                uid = response.id;
                                <?php if ($app->like == 1): ?>
                                FB.api({ method: 'fql.query', query: 'SELECT uid FROM page_fan WHERE uid= ' + uid + ' AND page_id=<?php echo($app->fanpage); ?>' },
                                    function(result) {
                                        if (result.length) {
                                            goToPage();
                                        } else {
                                            $(".modal-backdrop").show();
                                            $("#errorMsg").html('Aby kontynuować musisz polubić nasz profil!' );
                                            FB.Canvas.getPageInfo(function(info){
                                                $("#error").css('top',100+info.scrollTop+'px');
                                                $("#continue").hide();
                                                $("#errorOK").show();
                                                $("#error").fadeIn('fast');
                                            });
                                        }
                                    });
                                <?php else: ?>
                                goToPage();
                                <?php endif; ?>
                            });
                        } else {
                            console.log('User cancelled login or did not fully authorize.');
                        }
                    }, {scope: 'email,user_likes,user_about_me'});
                } else {
                    console.log('user is connected');
                    <?php if ($app->like == 1): ?>
                    if ( _liked ){
                        goToPage();
                    } else {
                        $(".modal-backdrop").show();
                        $("#errorMsg").html('Aby kontynuować musisz polubić nasz profil!' );
                        FB.Canvas.getPageInfo(function(info){
                            $("#error").css('top',150+info.scrollTop+'px');
                            $("#continue").hide();
                            $("#errorOK").show();
                            $("#error").fadeIn('fast');
                        });
                    }
                    <?php else: ?>
                    goToPage();
                    <?php endif; ?>
                }
            });
        });
    });
</script>