<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 11.06.13
 * Time: 12:00
 */
?>
<div id="scoresHolder">
    <?php if(!empty($answers)): ?>
        <?php
        switch($app->type){
            case 'quiz':
                echo '
                <div class="scoreLine">
                    <div class="scoreLp">&nbsp;</div>
                    <div class="scoreName">Imię i nazwisko</div>
                    <div class="scoreScore">Wynik</div>
                    <div class="scoreTime">Czas</div>
                </div>';
                    $lp = 0;
                    foreach( $answers as $answer ){
                        echo '<div class="scoreLine">';
                        echo '<div class="scoreLp">'.++$lp.'</div>';
                        echo '<div class="scoreName">'.$answer->user->first_name.' '.$answer->user->last_name.'</div>';
                        echo '<div class="scoreScore">'.$answer->sort_int.'</div>';
                        echo '<div class="scoreTime">'.$answer->data.'</div>';
                        echo '</div>';
                        if ($lp == 10)
                            break;
                    }
                break;
            case 'text':
                $questions = unserialize($app->questions);
                foreach ($answers as $part){
                    echo '<div class="scoreLine">';
                    echo '<div class="scoreName">'.$part->user->first_name.' '.$part->user->last_name.'</div>';
                    $answer = unserialize($part->sort_string);
                    switch ( $questions['texttype'] ){
                        case 's':
                            $dataContent = urlencode('<div class="modalQuestion">'.$questions[current(array_keys($answer))]['q'].'</div><div class="modalAnswer">'.current($answer).'</div>');
                            break;
                        case 'm':
                        default:
                            $tmp = '';
                            foreach ($questions as $id=>$question){
                                if ($id == 'texttype') continue;
                                $tmp .= '<div class="modalQuestion">'.$question['q'].'</div><div class="modalAnswer">'.$answer[$id].'</div>';
                            }
                            $dataContent = urlencode($tmp);
                    }
                    echo '<div class="scoreScore withDate"><span class="show_story" data-content="'.$dataContent.'"><img src="/images/textedit_16.png"/></div>';
                    echo '<div class="scoreTime withDate">'.$this->dateTimeFormat($part->created).'</div>';
                    echo '</div>';
                }
            break;
        }?>


    <?php else: ?>
        <div id="noScore">brak wyników</div>
    <?php endif; ?>
</div>