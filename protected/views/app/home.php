<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 11.06.13
 * Time: 11:21
 */
    $hidden = false;
    if ($app->answers != ''){
        $answers = unserialize($app->answers);
        if (isset($answers['hidden']) && $answers['hidden'] == 1){
            $hidden = true;
        }
    }

    if ( !Yii::app()->request->isAjaxRequest ):
?>
    <div id="header"><?php echo($app->name); ?></div>
    <div id="menu">
        <div class="menuItem first active" data-step="home"><?php echo($app->type=="quiz")?'Quiz':'Konkurs'; ?></div>
        <div class="menuItem" data-step="terms">Zasady</div>
        <div class="menuItem<?php echo($hidden)?' last':''; ?>" data-step="reward">Nagrody</div>
        <?php if (!$hidden): ?>
            <div class="menuItem last" data-step="scores"><?php echo($desc); ?></div>
        <?php endif; ?>
    </div>
    <?php if ( !empty($view['header'])): ?>
        <div id="appHeader"><img src="/<?php echo($view['header']); ?>"/></div>
    <?php endif; ?>
    <div id="appBody">
<?php endif; ?>
    <div id="questions">
        <?php echo($this->generate($app->type)); ?>
    </div>

<?php if ( !Yii::app()->request->isAjaxRequest ): ?>
    </div>
    <script type="text/javascript">var app = <?php echo (int)$app->id; ?>;</script>
<?php endif; ?>
