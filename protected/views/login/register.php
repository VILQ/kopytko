<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="pl" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/login.css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">
    <div id="content">
        <div id="header">Konkursy Facebook</div>
        <div id="errorMsg"><?php echo($msg); ?></div>
        <form method="post">
            <input class="cInput"  type="text" name="f_name" placeholder="imię"/><br/>
            <input class="cInput"  type="text" name="l_name" placeholder="nazwisko"/><br/>
            <input class="cInput"  type="text" name="mail" placeholder="mail"/><br/>
            <input class="cInput"  type="password" name="pass" placeholder="hasło"/><br/>
            <input type="hidden" name="action" value="register"/>
            <input class="cButton" type="submit" value="Załóż konto"/>
        </form>
        <?php echo(CHtml::link('powrót','/login',array('id'=>'registerLink'))); ?>
    </div>
</div><!-- page -->

</body>