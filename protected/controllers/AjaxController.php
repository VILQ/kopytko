<?php

class AjaxController extends Controller
{
    public function beforeAction($action){
        if (!Yii::app()->request->isAjaxRequest){
            die();
        } else {
            return TRUE;
        }
    }

	public function actionUpdate()
	{
        $post = $_POST;
        $url = $post['data']['id'].'?fields=picture';
		$data = json_decode(Curl::get($url));
        $user = User::model()->with('fbs')->find('email = "'.Yii::app()->user->id.'"');
        $fb = Fb::model()->find('user_id = '.$user->id);
        $fb->fb_id = $post['data']['id'];
        $fb->avatar = $data->picture->data->url;
        $fb->token = $post['token'];
        $fb->save();
        Yii::app()->session['avatar'] = $fb->avatar;
        Yii::app()->end();
	}

    public function actionPages()
    {
        $data_pages = $_POST['data']['data'];
        $user = User::model()->with('fbs')->find('email = "'.Yii::app()->user->id.'"');
        foreach($data_pages as $data_page){
            $page = Pages::model()->find('fb_id = '.$data_page['id'].' AND user_id = '.$user->id);
            if ( empty($page) ) {
                $page = new Pages();
                $page->fb_id = $data_page['id'];
                $page->user_id = $user->id;
            }
            $page->name = $data_page['name'];
            $page->access_token = $data_page['access_token'];
            $page->last = date("Y-m-d H:i:s");
            $page->save();
        }
        Yii::app()->end();
    }
    private function p($param) {
        return Yii::app()->request->getParam($param);
    }

    public function actionParticipants(){
        $user = Part::model()->find( 'fb_id = "'.$this->p('u_id').'" AND contest_id = '.$this->p('app_id'));
        if ( empty($user) ){
            $user = new Part();
            $user->contest_id = $this->p('app_id');
            $user->first_name = $this->p('u_fname');
            $user->last_name = $this->p('u_lname');
            $user->gender = $this->p('u_gender');
            $user->email = $this->p('u_mail');
            $user->fb_id = $this->p('u_id');
            $user->created = date("Y-m-d H:i:s");
            $user->terms = 1;
            $user->save();
            echo 'new user';
        } else {
            echo 'old user';
        }
    }
    public function actionStart(){
        $user = Part::model()->find( 'fb_id = "'.$this->p('u_id').'"');
        $timer = Timers::model()->findByPk($user->id.'|'.$this->p('app_id'));
        if ( empty($timer) ) {
            $timer = new Timers();
            $timer->id = $user->id.'|'.$this->p('app_id');
        }
        $timer->timer =  microtime(true);
        $timer->save();
        echo $timer->timer;
    }
    public function actionStop(){
        $user = Part::model()->find( 'fb_id = "'.$this->p('u_id').'"');
        $timer = Timers::model()->findByPk($user->id.'|'.$this->p('app_id'));
        $diff = number_format(microtime(true)-$timer->timer,3);
        $app = Contest::model()->findByPk($this->p('app_id'));
        $view = unserialize($app->view);
        $valid = unserialize($app->answers);
        $score = 0;
        $string = '';
        switch ( $app->type ){
            case 'quiz':
                $answers = $this->p('answers');
                $questions = unserialize($app->questions);
                foreach( $answers as $answer ){
                    $tmp = explode("|",$answer);
                    if ( $valid[$tmp[0]] == $tmp[1]) {
                        $score += $questions[$tmp[0]]['s'];
                    }
                }
                break;
            case 'text':
                    $tmp = array();
                    foreach ( $this->p('answers') as $answer ){
                        $a = explode("|",$answer);
                        $tmp[$a[0]] = $a[1];
                    }
                    $string = serialize( $tmp );
                break;
        }
        $submit = new Answers();
        $submit->user_id = $user->id;
        $submit->contest_id = $app->id;
        $submit->created = date("Y-m-d H:i:s");
        $timer = explode(",",gmdate("i:s",floor($diff)).','.current(explode(".",(($diff - floor($diff))*1000))));
        $submit->data = $timer[0].','.str_pad($timer[1],3,'0',STR_PAD_RIGHT);
        $submit->sort_int = $score;
        $submit->sort_float = $diff;
        $submit->sort_string = $string;
        if ($view['limit']['t'] != 'n'){
            $user->vote_lock = 1;
            $user->save();
        }
        $submit->save();
        echo $score;
    }

    public function actionInvites(){
        $contest = Contest::model()->findByPk($_POST['app_id']);
        $view = unserialize($contest->view);
        if ( $view['limit']['t'] == 'i' AND count($_POST['response']['to']) >= $view['limit']['v']){
            $user = Part::model()->find('fb_id = '.$_POST['u_id'].' AND contest_id = '.$_POST['app_id']);
            $user->vote_lock = 0;
            $user->save();
            echo 'ok';
        } else {
            echo 'error';
        }
    }

    public function actionSession(){
        if ( Yii::app()->request->isAjaxRequest ) {
            $tmp = unserialize(base64_decode(Yii::app()->session['fbparams']));
            $check = Contest::model()->findByPk( Yii::app()->request->getParam('id') );
            if ( $check->fanpage == $tmp['page']['id'] ){
                echo 'ok';
            } else {
                echo '//www.facebook.com/'.$check->fanpage.'/?sk=app_'.$this->apps[$check->type]['appId'];
            }
        }
    }
}