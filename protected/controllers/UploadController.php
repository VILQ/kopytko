<?php
ini_set( 'upload_max_filesize','64M' );
ini_set( 'post_max_size','64M' );
class UploadController extends Controller {
    
    private $fileName;
    
    private function parseSize( $size ) {
        $postfix = array(1 => 'B','kB','MB');
        foreach($postfix as $idx=>$val){
            if ( $size < ($idx * 1024) ){
                return $size.' '.$val;
            } else {
                $size = ceil( $size / ($idx * 1024) );
            }
        }
    }

	public function actionIndex() {
        if (isset($_GET['qqfile'])){
            $this->fileName = $_GET['qqfile'];
        } elseif (isset($_FILES['qqfile'])){
            $this->fileName = basename($_FILES['qqfile']['name']);

            if ($_FILES['qqfile']['size'] == 0){
                die ('{errors: "Przesłano pusty plik"}');
            }
            if ($_FILES['qqfile']['size'] == 2048000){
                die ('{errors: "Rozmiar zdjęcia przekracza 2MB"}');
            }
        } else {
            die ('{errors: "Nieprawidłowy plik zdjęcia.<br/>Dozwolone są pliki w formacie jpg lub png.(1)"}');
        }

        $file = new qqFileUploader(array('jpg','jpeg','png'));
        if ( !isset($_GET['view']) || empty($_GET['view'])) {
            $dir = '';
        } else {
            $dir = $_GET['view'].'s/';
        }
        $max_width = 790;
        if ( $dir = 'rewards/' ){
            $max_width = 250;
        }
        $save_path = 'images/'.$dir;
        $result = $file->handleUpload($save_path);
        if (isset($result['success'])){
            $target = $result['name'];
            $image = new Image();
            $image->load($target);
            if ( $image->get_width() > $max_width ) {
                $image->fit_to_width($max_width);
                $image->save($target);
            }
            $result['size'] = $this->parseSize($image->file_size());
            die( json_encode($result) );
        } else {
            die ('{errors: "Nieprawidłowy plik zdjęcia.<br/>Dozwolone są pliki w formacie jpg lub png.('.$dir.')"}');
        }
	}
}