<?php
/**
 * Created by JetBrains PhpStorm.
 * User: VILQ
 * Date: 15.06.13
 * Time: 17:07
 */

class PdfController extends Controller  {

    public function actionIndex(){

        if ( isset ( $_POST['header'] ) ) {
            $post = $_POST;
            $html = '<style>'.$post['css'].'</style>'.$post['header'];
            $strip_from = array('&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, Arial, Helvetica, sans-serif;font-size:12px;','&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Verdana, Arial, Helvetica, sans-serif;font-size:16px;','<path fill="#E0E0E0" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" stroke="#666"');
            $strip_to = array('DejaVuSans;font-size:11px;','DejaVuSans;font-size:14px;','<path fill="#FFFFFF" d="M 6 6.5 L 20 6.5 M 6 11.5 L 20 11.5 M 6 16.5 L 20 16.5" stroke="#FFF"');
            $pdf = new TCPDF('landscape', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetTitle($post['app_name'].'-'.$post['fb_name'].'_('.$post['start'].'-'.$post['stop'].')');
            $pdf->SetHeaderData();
            $pdf->setFooterData();
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->SetDefaultMonospacedFont('dejavusans');
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->AddPage();
            $pdf->SetY(10);
            $pdf->SetFont('dejavusans', '', 14);
            for( $i = 1; $i <= 4; $i++){
                $pdf->writeHTML($html, true, false, true, false, 'C');
                $pdf->ImageSVG('@'.str_replace($strip_from,$strip_to,$post['chart'.$i]), 2, 0, 480, 300, '', '', '', 0, false);
                $pdf->SetY(155);
                $pdf->writeHTML('<style>'.str_replace('font-size: 14px;','font-size: 11px;',$post['css']).'</style><div class="comment">'.$post['comment'.$i].'</div>', true, false, true, false, 'J');
                if ($i != 4){
                    $pdf->AddPage();
                } else {
                    $pdf->lastPage();
                }
            }
            $pdf->Output($post['app_name'].'-'.$post['fb_name'].'_('.$post['start'].'-'.$post['stop'].').pdf', 'I');
        }

    }

}