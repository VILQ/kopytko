<?php

class RejectController extends Controller
{
    public function actionIndex()
    {
        $code = Yii::app()->request->getParam('code');
        $check = User::model()->find('confirm = "'.$code.'"');
        if ( !empty($check) ){
            $check->delete();
            die('Konto zostało usunięte.');
        } else {
            die('Nieprawidłowy kod potwierdzenia.');
        }
    }
}