<?php

class CronController extends Controller
{
	private function getFbData( $fanpage = '' )
	{
        $curl = curl_init('https://graph.facebok.com/'.$fanpage);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = json_decode(curl_exec($curl));
        if ( isset($output->error)){
            return 0;
        } else {
            return $output->likes;
        }
    }

	public function actionGraph()
	{
        set_time_limit('3600');
        $date = new DateTime();
        $now = $date->format("Y-m-d");
        $date->modify('-1 day');
        $yesterday = $date->format("Y-m-d");;
		$pages = Pages::model()->findAll('1 GROUP BY fb_id');
        foreach ( $pages as $page ){
            $like = PagesLike::model()->find('fb_id = '.$page->fb_id.' AND date = "'.$now.'"');
            $is_new = PagesLike::model()->find('fb_id = '.$page->fb_id);
            if ( empty($like) ) {
                $like = new PagesLike();
                $like->fb_id = $page->fb_id;
                $like->date = $now;
            }
            if ( empty($is_new) ){
                $like->date = $yesterday;
            } else {
                $like->date = $now;
            }
            $like->likes = $this->getFbData( $page->fb_id );
            $like->last_updated = date("Y-m-d H:i:s");
            $like->save();
        }
	}

    public function actionIndex(){
        die();
    }
}