<?php

class AppController extends Controller
{

    public $page_id;
    private $contest;
    private $type;
    private $check;
    private $ended = false;
    public $terms_url = 'about:blank';

    private function checkAppId( $appId ){
        $valid = false;
        foreach ( $this->apps as $key => $app ) {
            if ( $app['appId'] == $appId ) {
                $this->type = $key;
                $valid = true;
            }
        }
        return $valid;
    }

    private function errorMsg($msg){
        return '<div style="padding:200px 0px; text-align:center; font-family:Arial; font-size:16px">'.$msg.'</div>';
    }

    public function dateTimeFormat( $date , $format = "d.m.Y H:i"){
        $date = new DateTime( $date );
        return $date->format($format);
    }

    public function beforeAction($action){
        if ( !$this->checkAppId( Yii::app()->request->getParam('id') ) ) {
            die($this->errorMsg('Aplikacja nie została rozpoznana'));
        }
        $facebook = new Facebook(
            array(
                'appId'  => $this->apps[$this->type]['appId'],
                'secret' => $this->apps[$this->type]['secret']
            )
        );
        $request = $facebook->getSignedRequest();
        if ( isset( $request['page']) ) {
            $this->check = $request;
            Yii::app()->session['fbparams'] = base64_encode(serialize($request));
        } else {
            $this->check = unserialize(base64_decode(Yii::app()->session['fbparams']));
        }
        if ( isset( $this->check['page']['id'] ) ) {
            $this->page_id = $this->check['page']['id'];
            $this->contest = Contest::model()->find('app_id = "'.Yii::app()->request->getParam('id').'" AND fanpage = "'.$this->page_id.'" AND data != "deleted"');
            if( $this->check['page']['admin'] != 1 && $this->contest->sandbox == 1) {
                die($this->errorMsg('Aplikacja niedostępna'));
            }
            if ( empty($this->contest) ){
                die($this->errorMsg('Aplikacja nie została odnaleziona'));
            }
            $now = date("Y-m-d H:i:s");
            if ( $this->contest->date_start > $now ){
                die($this->errorMsg('Aplikacja nie została jeszcze uruchomiona'));
            }
            if ( $this->contest->date_end < $now ){
                $this->ended = true;
            }
        } else {
            die($this->errorMsg('Strona aplikacji nie została rozpoznana'));
        }
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile('/js/jquery.cookie.js');
        Yii::app()->clientScript->registerScriptFile('//connect.facebook.net/pl_PL/all.js');
        Yii::app()->clientScript->registerScriptFile('/js/app.js');
        return TRUE;
    }



    public function actionLoad(){
        $this->layout = '//layouts/column2';
        $view = unserialize($this->contest->view);
        if ( !empty($view['terms_url'])) {
            $this->terms_url = $view['terms_url'];
        }
        $answers = '';
        $desc = '';
        if ( !empty($view['css']) ) {
            Yii::app()->clientScript->registerCss("a".$this->contest->fanpage,$view['css'],'all');
        }
        Yii::app()->clientScript->registerCssFile("/css/modals.css",'all');
        switch ( $this->contest->type ) {
            case 'text':
                $desc = 'Odpowiedzi';
                $params = $this->contest->params();
                if ( Yii::app()->request->getParam('view')=='scores' ) {
                    if ( isset($params['sort']) ){
                        if ( $this->contest->type == "quiz"){
                            $answers = Answers::model()->with('user')->findAllBySql('select * from answers where contest_id = '.$this->contest->id.' order by answers.'.$params['sort']);
                        } else {
                            $answers = Answers::model()->with('user')->findAllBySql('select * from answers where contest_id = '.$this->contest->id.' order by '.$params['sort']);
                        }
                    } else {
                        $answers = Answers::model()->with('user')->findAll('contest_id = '.$this->contest->id);
                    }
                }
                break;
            case 'photo':
                $desc = 'Zgłoszenia';
                break;
            case 'quiz':
                $desc = 'Wyniki';
                $params = $this->contest->params();
                if ( Yii::app()->request->getParam('view')=='scores' ) {
                    if ( isset($params['sort']) ){
                        if ( $this->contest->type == "quiz"){
                            $answers = Answers::model()->with('user')->findAllBySql('select * from (select * from answers where contest_id = '.$this->contest->id.' order by '.$params['sort'].') as m group by user_id order by m.sort_int DESC');
                        } else {
                            $criteria = new CDbCriteria();
                            $criteria->addCondition('t.contest_id = '.$this->contest->id);
                            $criteria->order = $params['sort'];
                            $answers = Answers::model()->with('user')->findAll($criteria);
                        }
                    } else {
                        $answers = Answers::model()->with('user')->findAll('contest_id = '.$this->contest->id);
                    }
                }
                break;
            default:
        }
        $this->layout = '//layouts/column2';
        $load = Yii::app()->request->getParam('view');
        $fb_params = unserialize(base64_decode(Yii::app()->session['fbparams']));
        if ($fb_params['user_id'] != '') {
            $userVote = Part::model()->find('contest_id = '.$this->contest->id.' AND fb_id = '.$fb_params['user_id']);
        } else {
            $userVote = array();
        }
        if (!empty($userVote) && $userVote->vote_lock != 0 && $load =='home' && $view['limit']['t'] != 'n'){
            $load = 'limit';
            if ( $view['limit']['t']=='d' ){
                 $last = Answers::model()->find('user_id = '.$userVote->id.' AND contest_id = '.$this->contest->id.' ORDER BY created DESC');
                 $voted = new DateTime( $last->created );
                 $now = new DateTime();
                 //$diff = $now->diff($voted)->format('%R%a'); //PHP 5.3.0+ :(
                if ( ($now->format('U') - $voted->format('U')) >= 86400 ){
                    $userVote->vote_lock = 0;
                    $userVote->save();
                    $load = 'home';
                }
            }
        }
        if ( Yii::app()->request->isAjaxRequest ) {
            $this->renderPartial($load,
                array(
                    'app' => $this->contest,
                    'desc' => $desc,
                    'view' => $view,
                    'answers' => ( $load=='scores' )?$answers:'',
                    'liked' => (bool)$this->check['page']['liked']
                )
            );
        } else {
            Yii::app()->clientScript->registerScriptFile('/js/app_home.js');
            $this->render( (!$this->ended)?$load:'finish',
                array(
                    'app' => $this->contest,
                    'desc' => $desc,
                    'view' => $view,
                    'liked' => (bool)$this->check['page']['liked']
                )
            );
        }
    }

    public function checkUserConnected(){
        $check = Part::model()->find('contest_id = "'.$this->contest->id.'" and fb_id="'.$this->check['user_id'].'"');
        if ( !empty($check) ){
            $this->redirect('/home/'.$this->contest->app_id);
        }
    }

	public function actionIndex()
	{
        $this->checkUserConnected();
        $view = unserialize($this->contest->view);
        if ( !empty($view['css']) ) {
            Yii::app()->clientScript->registerCss("a".$this->contest->fanpage,$view['css'],'all');
        }
        Yii::app()->clientScript->registerCssFile("/css/modals.css",'all');
        switch ( $this->contest->type ) {
            case 'text':
                $desc = 'Odpowiedzi';
                break;
            case 'photo':
                $desc = 'Zgłoszenia';
                break;
            default:
                $desc = 'Wyniki';
                break;
        }
        $this->layout = '//layouts/column2';
		$this->render('index',
                        array(
                            'app' => $this->contest,
                            'desc' => $desc,
                            'view' => $view,
                            'liked' => (bool)$this->check['page']['liked']
                        )
                    );
	}

    private function shuffle_assoc(&$array) {
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return true;
    }

    public function aasort(&$array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }

    public function generate( $type = 'quiz' ) {
        $questions = unserialize($this->contest->questions);
        $output = '';
        switch ( $type ) {
            case 'quiz':
                $this->shuffle_assoc($questions);
                $output .= '<div id="quizHolder"><div class="qHolder" id="qsh"><span class="btn2">START</span></div>';
                $cur = 0;
                foreach ( $questions as $id=>$question ) {
                    if ($id == 'limit' || $cur >= $questions['limit']) continue;
                    $output .= '<div class="qHolder" id="h'.++$cur.'">';
                    $output .= '<div class="qQuestion" id="q'.$id.'">'.$question['q'].'</div>';
                    $this->shuffle_assoc($question['a']);
                    foreach ( $question['a']  as $a=>$answer ){
                        $output .= '<div class="qAnswer" id="a'.$id.'|'.$a.'"><input type="radio" name="q'.$id.'" value="'.$a.'"/><label for="'.$id.'|'.$a.'">'.$answer.'</label></div>';
                    }
                    $output .= '<span class="nextQuestion" id="n'.($cur+1).'" data-q="q'.$id.'">Dalej</span>';
                    $output .= '</div>';
                }
                $output .= '<div class="qHolder finish" id="h'.($cur+1).'">Dziękujemy za udział<br/>Twój wynik to: <span id="finishScore"><img src="/images/spinner_small.gif"/></span> pkt.</div>';
                $output .= '</div>';
                break;
            case 'text':
                    switch($questions['texttype']){
                        case 's':
                            $output .= '<div id="quizHolder"><div class="qHolder" id="qsh"><span class="btn2">START</span></div>';
                            $output .= '<div class="qHolder" id="h1">';
                            $output .= '<select id="qSelect"><option value="0" DISABLED SELECTED>-- wybierz z listy --</option>';
                            foreach ( $questions as $id=>$question ) {
                                if ($id == 'texttype' ) continue;
                                $output .= '<option value="q'.$id.'">'.$question['q'].'</option>';
                            }
                            $output .= '</select>';
                            $output .= '<textarea name="q1" id="text1" class="cArea"/></textarea>';
                            $output .= '<span class="nextQuestion finish" id="n2" data-q="0">Wyślij</span></div>';
                            $output .= '<div class="qHolder finish" id="h2">Dziękujemy za wzięcie udziału w konkursie.</div>';
                            $output .= '</div>';
                            break;
                        case 'm':
                        default:
                        $output .= '<div id="quizHolder"><div class="qHolder" id="qsh"><span class="btn2">START</span></div>';
                        $cur = 0;
                        foreach ( $questions as $id=>$question ) {
                            if ($id == 'texttype' ) continue;
                            $output .= '<div class="qHolder" id="h'.++$cur.'">';
                            $output .= '<div class="qQuestion text" id="q'.$id.'">'.$question['q'].'</div>';
                            $output .= '<textarea name="q'.$id.'" id="text'.$id.'" class="cArea"/></textarea>';
                            $output .= '<span class="nextQuestion" id="n'.($cur+1).'" data-q="q'.$id.'">Dalej</span>';
                            $output .= '</div>';
                        }
                        $output .= '<div class="qHolder finish" id="h'.($cur+1).'">Dziękujemy za wzięcie udziału w konkursie.</div>';
                        $output .= '</div>';
                    }
                break;
            default:
        }
        return $output;
    }
}