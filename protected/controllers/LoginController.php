<?php

class LoginController extends Controller
{
	public function actionIndex()
	{
        $post = $_POST;
        $msg = '';
        if ( !empty($post) ) {
            if ( isset($post['name']) && isset($post['pass']) && !empty($post['name']) && !empty($post['pass'])){
                $check = User::model()->with('fbs')->find('email = "'.$post['name'].'" AND password = "'.md5($post['pass']).'"');
                if ( empty($check) ) {
                    $msg = 'Nieprawidłowe dane logowania';
                } elseif ( $check->confirm != '') {
                    $msg = 'Konto nie zostało poprawnie aktywowane';
                } else {
                    //Yii::app()->session['userIsLogged'] = true;
                    $user = new UserIdentity($post['name'],$post['pass']);
                    $now = date("Y-m-d H:i:s");
                    if ($check->valid_until == NULL) {
                        $date = new DateTime($check->created);
                        $date->modify('+14 days');
                        $check->valid_until = $date->format("Y-m-d H:i:s");
                    }
                    if ( $check->valid_until < $now) {
                        if ($check->type != 'admin'){
                            $check->type = 'posttrial';
                        }
                    }
                    $check->last = $now;
                    $check->save();
                    Yii::app()->user->login($user,3600*24*7);
                    Yii::app()->session['user_id'] = $check->id;
                    Yii::app()->session['username'] = $check->first_name.' '.$check->last_name;
                    Yii::app()->session['avatar'] = $check->fbs[0]->avatar;
                    Yii::app()->session['fb_id'] = $check->fbs[0]->fb_id;
                    $this->redirect('/panel');
                }
            } else {
                $msg = 'Nieprawidłowe dane logowania';
            }
        }
		$this->renderPartial('index',array('msg'=>$msg));
	}

	public function actionRegister()
	{
        if ( !isset ( $_POST['action'] ) || $_POST['action'] != 'register' ) {
            $this->renderPartial('register',array('msg'=>''));
        } else {
            if ( $this->validate($_POST) ) {
                $this->renderPartial('thanks');
            } else {
                $this->renderPartial('register',
                    array( 'msg'=> 'Istnieje już konto zarejestrowane na podany adres email.',
                        'data'  => $_POST )
                );
            }
        }
	}
    private function validate( $data ) {
        $check = User::model()->find('email = "'.$data['mail'].'"');
        if ( !empty($check) ) {
            return false;
        } else {
            $user = new User();
            $user->first_name = $data['f_name'];
            $user->last_name  = $data['l_name'];
            $user->email      = $data['mail'];
            $user->password   = md5($data['pass']);
            $user->created    = date("Y-m-d H:i:s");
            $user->valid_until = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s"), date("m"), date("d")+14, date("Y")));
            $user->confirm    = md5( $user->email.$user->created );
            $user->save();
            $this->sendConfirm( $data, $user->confirm );
            $fb = new Fb();
            $fb->user_id = $user->id;
            $fb->save();
            return true;
        }
    }

    private function sendConfirm( $data, $hash = '' ) {
        $url = array(
            'ok' => Yii::app()->createAbsoluteUrl('/confirm//code/'.$hash),
            'no' => Yii::app()->createAbsoluteUrl('/reject//code/'.$hash)
        );
        $message            = new YiiMailMessage;
        //this points to the file test.php inside the view path
        $message->view = "new_account";
        $message->subject    = 'Konkursy Facebook - Nowe konto. Potwierdzenie rejestracji.';
        $message->setBody(array('data'=> $data, 'url' => $url), 'text/html');
        $message->addTo($data['mail']);
        $message->from = Yii::app()->params['adminEmail'];
        Yii::app()->mail->send($message);
    }
}