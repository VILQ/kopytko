<?php

class SiteController extends Controller
{

    public function beforeAction($action){
        if (empty(Yii::app()->user->id)){
            $this->redirect('/login');
        }
        return TRUE;
    }

	public function actionIndex()
	{
        if ( empty(Yii::app()->user->id) ) {
            $this->redirect('/login');
        } else {
		    $this->redirect('/panel');
        }
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}