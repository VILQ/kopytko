<?php

class PanelController extends Controller
{
    public $msg;

    public function dateTimeFormat( $date , $format = "d.m.Y H:i"){
        $date = new DateTime( $date );
        return $date->format($format);
    }

    public function dateFormat( $date, $format = "d.m.Y" ){
        $date = new DateTime( $date );
        return $date->format($format);
    }
    public function beforeAction($action){
        if (empty(Yii::app()->user->id) || empty(Yii::app()->session['user_id'])){
            $this->redirect('/login');
        }
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerScriptFile('/js/jquery.cookie.js');
        Yii::app()->clientScript->registerScriptFile('//connect.facebook.net/pl_PL/all.js');
        Yii::app()->clientScript->registerScriptFile('/js/main.js');
        Yii::app()->clientScript->registerScriptFile('/js/highcharts.js');
        Yii::app()->clientScript->registerScriptFile('/js/exporting.js');
        Yii::app()->clientScript->registerScriptFile('/js/newapp.js');
        Yii::app()->clientScript->registerScriptFile('/js/fileuploader.js');
        Yii::app()->language = 'pl';
        return TRUE;
    }
	public function actionIndex()
	{
        $user = User::model()->findByPk(Yii::app()->session['user_id']);
        if ($user->type == 'posttrial'){
            $this->msg .= '14 dniowy okres testowy został zakończony.';
        }
        if ( Yii::app()->request->isAjaxRequest ){
            $this->renderPartial('index');
        } else {
		    $this->render('index');
        }
	}
    public function actionNewapp(){
        if ( Yii::app()->request->isAjaxRequest ){
            $this->renderPartial('newapp');
        } else {
            $this->render('newapp');
        }

    }
    public function actionNew(){
        if ( Yii::app()->request->isAjaxRequest ){
            if ( !isset($_POST['newapp']) ) {
                $app = Contest::model()->find('user_id = '.Yii::app()->session['user_id'].' AND data="temp" AND type="'.Yii::app()->request->getParam('type').'"');
                $check = Contest::model()->findAll('user_id = '.Yii::app()->session['user_id'].' AND data="published" AND type="'.Yii::app()->request->getParam('type').'"');
                $appList = array();
                if ( !empty($check) ) {
                    foreach ($check as $chk) {
                        $appList[] = $chk->fanpage;
                    }
                }
                if (empty($app)) {
                    $app = new Contest();
                    $app->user_id = Yii::app()->session['user_id'];
                    $app->app_id = $this->apps[Yii::app()->request->getParam('type')]['appId'];
                    $app->type = Yii::app()->request->getParam('type');
                    $app->data = 'temp';
                    $app->save();
                    $params = new ContestParams();
                    $params->contest_id = $app->id;
                    $params->name = 'sort';
                    $params->value = 'id DESC';
                    $params->save();
                    $fp = User::model()->with('pages')->findAllByPk($app->user_id);
                    echo $this->renderPartial('new'.Yii::app()->request->getParam('type'), array('app' => $app, 'user' => $fp, 'check' => $appList ), true, true);
                } else {
                    $this->renderPartial('temp',array('type'=>$app->type,'app_id'=>$app->id));
                }
            }
        }
    }
    public function actionEdit(){
        if ( Yii::app()->request->isAjaxRequest ){
            $app = Contest::model()->with('contestParams')->findByPk(Yii::app()->request->getParam('app_id'));
            $fp = User::model()->with('pages')->findAllByPk($app->user_id);
            $params = $app->params();
            if ($app->type == 'quiz'){
                $part = Answers::model()->with('user')->findAllBySql('select * from (select * from answers where contest_id = '.$app->id.' order by '.$params['sort'].') as m group by user_id order by m.sort_int DESC');
            } else {
                $part = Answers::model()->with('user')->findAllBySql('select * from answers where contest_id = '.$app->id.' order by '.$params['sort']);
            }
            $check = Contest::model()->findAll('user_id = '.Yii::app()->session['user_id'].' AND data="published" AND type="'.$app->type.'"');
            $appList = array();
            if ( !empty($check) ) {
                foreach ($check as $chk) {
                    $appList[] = $chk->fanpage;
                }
            }
            echo $this->renderPartial('edit'.$app->type, array('app' => $app, 'user' => $fp, 'participants' => $part,'check'=>$appList), true, true);
        }
    }
    public function prepareEdit( $app, $key ){
        $tmp = unserialize($app->{$key});
        if ( !empty( $tmp ) ){
            return $tmp;
        } else {
            return array();
        }
    }
    public function actionCreate(){
        if ( !empty($_POST) && isset($_POST['id'])){
            $post = $_POST;
            $contest = Contest::model()->findByPk($post['id']);
            $contest->fanpage = $post['appFp'];
            $contest->name = $post['appName'];
            $contest->sandbox = (int)($post['sandbox']=='on');
            $contest->like = (int)($post['appLike']=='on');
            $contest->info = urlencode( $post['info'] );
            $contest->view = serialize( $post['view'] );
            $contest->questions = serialize( $post['questions'] );
            $contest->answers = serialize( $post['good'] );
            $contest->rewards = serialize( $post['rewards'] );
            $contest->date_start = $this->dateTimeFormat($post[ $post['type'].'_start' ],"Y-m-d H:i:s");
            $contest->date_end   = $this->dateTimeFormat($post[ $post['type'].'_end' ],"Y-m-d H:i:s");
            $contest->terms = urlencode( str_replace(array('$LIMIT$','$PYTAN$','$START$','$STOP$'),array($post['questions']['limit'],count($post['questions'])-1,$this->dateTimeFormat($contest->date_start),$this->dateTimeFormat($contest->date_end)),$post['terms']) );
            $contest->type = $post['type'];
            if ( !isset($post['preview']) || empty($post['preview'])){
                $contest->data = 'published';
            }
            $contest->save();
            switch ( $contest->type ) {
                case 'text':
                    $params = ContestParams::model()->findByPk($contest->id);
                    if ( empty($params) ){
                        $params = new ContestParams();
                        $params->contest_id = $contest->id;
                    }
                    $params->name = 'sort';
                    $params->value = 'created DESC';
                    $params->save();
                    break;
                case 'quiz':
                    $params = ContestParams::model()->findByPk($contest->id);
                    if ( empty($params) ){
                        $params = new ContestParams();
                        $params->contest_id = $contest->id;
                    }
                    $params->name = 'sort';
                    $params->value = 'sort_int DESC, sort_float ASC';
                    $params->save();
                    break;
                default:
            }
            if ( !isset($post['preview']) || empty($post['preview']) ){
                $this->msg = 'Zapisano';
                $this->redirect('/panel');
            } else {
                $this->renderPartial('preview',array('app'=>$contest));
            }
        } else {
            $this->redirect('/panel');
        }

    }
    public function actionRemove(){
        if ( Yii::app()->request->isAjaxRequest && isset($_POST['app'])){
            $app = Contest::model()->with('contestParams')->find('user_id = '.Yii::app()->session['user_id'].' AND id = '.$_POST['app']);
            $app->delete();
            $this->redirect('/panel/new?type='.$_POST['type']);
        } else {
            echo 'error';
        }

    }
    public function actionDelete(){
        if ( Yii::app()->request->isAjaxRequest && isset($_POST['app'])){
            $app = Contest::model()->find('user_id = '.Yii::app()->session['user_id'].' AND id = '.$_POST['app']);
            $app->date_end = date('Y-m-d H:i:s');
            $app->data='deleted';
            $app->save();
            echo 'ok';
        } else {
            echo 'error';
        }
    }
    public function actionMyapps(){
        $this->renderPartial('applist',
            array(
                'mode' => 'edit',
                'info' => 'Edytuj',
                'contests'=> Contest::model()->findAll( 'user_id = '.Yii::app()->session['user_id'].' AND data = "published" ORDER BY id DESC' )
            )
        );
    }

    public function actionArchive(){
        $this->renderPartial('applist',
            array(
                'mode' => 'archived',
                'info' => 'Zobacz',
                'contests'=> Contest::model()->findAll( 'user_id = '.Yii::app()->session['user_id'].' AND data = "deleted" ORDER BY id DESC' )
            )
        );
    }
    public function actionArchived(){
        $app = Contest::model()->with('answers0')->with('user')->with('parts')->with('contestParams')->findByPk( Yii::app()->request->getParam('app_id') );
        $fp = Pages::model()->find('fb_id ='.$app->fanpage);
        $params = $app->params();
        if ($app->type == 'quiz'){
            $part = Answers::model()->with('user')->findAllBySql('select * from (select * from answers where contest_id = '.$app->id.' order by '.$params['sort'].') as m group by user_id order by m.sort_int DESC');
        } else {
            $part = Answers::model()->with('user')->findAllBySql('select * from answers where contest_id = '.$app->id.' order by '.$params['sort']);
        }
        $this->renderPartial('archive',
            array(
                'app' => $app,
                'fp' => $fp,
                'participants' => $part
            )
        );
    }
    public function actionStats(){
        $this->renderPartial('applist',
            array(
                'mode' => 'appStats',
                'info' => 'Pokaż statystyki',
                'contests'=> Contest::model()->findAll( 'user_id = '.Yii::app()->session['user_id'].' AND data = "published" OR data = "deleted" ORDER BY id DESC' )
            )
        );
    }

    public function actionAppStats(){
        $app_id = Yii::app()->request->getParam('app_id');
        $app = Contest::model()->findByPk( $app_id );
        $fp = Pages::model()->find('fb_id = '.$app->fanpage);
        $this->renderPartial('stats',
            array(
                'stats' => $this->generateStats( $app_id ),
                'app' => $app,
                'fp' => $fp
            )
        );
    }
    public function createUploader($id = 'default', $desc = '', $array = 'view', $edit = false, $photo='',$subArray = ''){
        echo '
        <div class="jUploader" data-target="'.$array.'" id="u_'.$id.'" '.(($edit)?'style="display:none"':'').'></div>
                <div class="preview p_'.$array.'" id="p_'.$id.'" '.(($edit)?'style="display:block"':'').'>
                    <div class="previewPhoto" id="pp_'.$id.'">
                        <img src="/'.$photo.'"/>
                    </div>
                    <div class="previewInfo" id="pi_'.$id.'">
                        <div class="delInfo cButton" class="cButton" data-id="'.$id.'">
            Usuń plik <span id="ps_'.$id.'"></span>
                        </div>
                    </div>
                </div>
                <div id="l_'.$id.'" class="file-limit" '.(($edit)?'style="display:none"':'').'>'.$desc.'</div>
                <input type="hidden" id="f_'.$id.'" name="'.$array.'['.$id.']'.$subArray.'" value="'.$photo.'"/>';
    }
    public function actionNewreward(){
        if ( Yii::app()->request->isAjaxRequest && isset($_POST['reward'])){
            echo '<div class="newReward" id="new_reward'.$_POST['reward'].'">';
            echo '<hr/>';
            $this->createUploader('reward'.$_POST['reward'],'(max. szerokość 250px, max. rozmiar 2MB)','rewards',false,'','[photo]');
            echo '<label>Nagroda za miejsce:</label><input type="text" class="qScore pReward" placeholder="#" name="rewards[reward'.$_POST['reward'].'][place]"/><br/>';
            echo '<label>Nazwa nagrody:</label><input type="text" class="cInput qReward" placeholder="Nazwa nagrody" name="rewards[reward'.$_POST['reward'].'][name]""/>';
            echo '<label>Krótki opis:</label><input type="text" class="cInput qReward" placeholder="Opis nagrody" name="rewards[reward'.$_POST['reward'].'][desc]"/>';
            echo '<br/><span class="cButton delReward">Usuń nagrodę</span><br/>';
            echo '</div>';
        }
    }
    public function actionDeletevote(){
        if ( Yii::app()->request->isAjaxRequest && isset($_POST['id'])){
            $answer = Answers::model()->with('contest')->findByPk($_POST['id']);
            if ( !empty($answer) ){
                $answer->delete();
                $part = Answers::model()->with('user')->findAllBySql('select * from (select * from answers where contest_id = '.$this->contest->id.' order by sort_int desc, sort_float asc) as m group by user_id order by m.sort_int DESC');
                echo $this->renderPartial('scores'.$answer->contest->type,array('participants'=>$part,'rewards'=>unserialize($answer->contest->rewards)),true,true);
            }
        }
    }

    private function generateRange($from,$to){
        $result = array();
        $start = new DateTime($from);
        while ($start->format('Y-m-d') <= $to ){
            $result[] = $start->format('Y-m-d');
            $start->modify('+1 day');
        }
        return $result;
    }

    private function getStats($model = '', $field = 'contest_id', $field_id = '', $range = array()){
        $result = array();
        $first = $this->dateFormat(current($range));
        $total = 0;
        if ( $model != '' and $field_id != '' and !empty($range) ){
            foreach ( $range as $date ){
                $query = Yii::app()->db->createCommand()
                    ->select('COUNT(*)')
                    ->from($model)
                    ->where(
                        array( 'and', $field.' = '.$field_id,
                            array( 'and', 'created like "'.$date.'%"')
                        )
                    )->queryAll();
                $result['growth'][$this->dateFormat($date)] = current(current($query));
                $total += current(current($query));
                $result['sum'][$this->dateFormat($date)] = $total;
            }
        }
        unset ($result['sum'][$first]);
        unset ($result['growth'][$first]);
        return $result;
    }

    private function getStatsHour($contest_id = ''){
        $result = array();
        $range = range('00','23');
        if ( $contest_id != '' and !empty($range) ){
            foreach ( $range as $date ){
                $query = Yii::app()->db->createCommand()
                    ->select('COUNT(*)')
                    ->from('answers')
                    ->where(
                        array( 'and', 'contest_id = '.$contest_id,
                            array( 'and', 'created like "% '.str_pad($date,2,'0',STR_PAD_LEFT).':%"')
                        )
                    )->queryAll();
                $result['growth'][$date.':00'] = current(current($query));
            }
        }
        return $result;
    }

    public function arrayToSeries( $array ){
        return '"'.implode('","',$array).'"';
    }

    private function getLikes( $fb_id = '', $range = array() ){
        $result = array();
        $first = $this->dateFormat(current($range));
        $last = 0;
        if ( $fb_id != '' and !empty($range) ){
            foreach ( $range as $date ){
                $criteria = new CDbCriteria();
                $criteria->addCondition('fb_id = '.$fb_id);
                $criteria->addCondition('last_updated like "'.$date.'%"');
                $fp = PagesLike::model()->find($criteria);
                $result['sum'][$this->dateFormat($date)] = isset($fp->likes)?$fp->likes:0;
                if ( $date == $range[0] ){
                    $result['growth'][$this->dateFormat($date)] = $last;
                    $last = $result['sum'][$this->dateFormat($date)];
                } else {
                    $result['growth'][$this->dateFormat($date)] = $result['sum'][$this->dateFormat($date)]-$last;
                    $last = $result['sum'][$this->dateFormat($date)];
                }
            }
        }
        unset ($result['sum'][$first]);
        unset ($result['growth'][$first]);
        return $result;
    }

    private function generateStats( $app_id = '' ){

        $app = Contest::model()->with('answers0')->findByPk( $app_id );
        $fp = Pages::model()->find('fb_id = '.$app->fanpage);

        $date = new DateTime($app->date_start);
        $date->modify('-1 day');
        $from = $date->format('Y-m-d');
        $date = new DateTime($app->date_end);
        $to = $date->format('Y-m-d');
        if ( $to > date("Y-m-d") ){
            $to = date("Y-m-d");
        }
        $range = $this->generateRange($from,$to);

        $users = $this->getStats('part','contest_id',$app->id,$range);
        $users['title'] = $app->name." (użytkownicy)";
        $users['column'] = "Dzienny wzrost użytkowników";
        $users['spline'] = "Całkowita ilość użytkowników";

        $answers = $this->getStats('answers','contest_id',$app->id,$range);
        $answers['title'] = $app->name." (zgłoszenia / dzień)";
        $answers['column'] = "Dzienny wzrost zgłoszeń";
        $answers['spline'] = "Całkowita ilość zgłoszeń";

        $hourly = $this->getStatsHour($app->id);
        $hourly['title'] = $app->name." (zgłoszenia / godzina)";
        $hourly['column'] = "Zgłoszenia";

        $likes = $this->getLikes($app->fanpage,$range);
        $likes['title'] = $fp->name." (fani)";
        $likes['column'] = "Dzienny wzrost fanów";
        $likes['spline'] = "Całkowita ilość fanów";

        return array(
                        'users' => $users,
                        'answers' => $answers,
                        'hourly' => $hourly,
                        'likes' => $likes,
                    );
    }
}