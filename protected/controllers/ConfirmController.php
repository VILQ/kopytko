<?php

class ConfirmController extends Controller
{
	public function actionIndex()
	{
        $code = Yii::app()->request->getParam('code');
        $check = User::model()->find('confirm = "'.$code.'"');
        if ( !empty($check) ){
            $check->confirm = '';
            $check->save();
            $this->renderPartial('index');
        } else {
            die('Nieprawidłowy kod potwierdzenia');
        }
	}
}