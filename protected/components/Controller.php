<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public $apps = array (
        'quiz' => array(
            'appId' => '256812304461309',
            'secret' => '202e0ea8f72f2f3c419ef554c0d3256d'
        ),
        'text' => array(
            'appId' => '141853992677848',
            'secret' => '82048861b89b0d69d5e521753368a863'
        ),
        'photo' => array(
            'appId' => '256812304461309',
            'secret' => '202e0ea8f72f2f3c419ef554c0d3256d'
        ),
        'memo' => array(
            'appId' => '256812304461309',
            'secret' => '202e0ea8f72f2f3c419ef554c0d3256d'
        ),
    );

	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
}